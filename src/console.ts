import fs from "fs";
import ConsoleCommandsService from "./core/classes/services/ConsoleCommandsService";
import InitService from "./core/classes/services/InitService";

function exit(err) {
    if (err) { console.error(err.stack); }
    setTimeout(() => process.exit(err ? 1 : 0), 1000);
}

(async () => {
    InitService.initApp("console");
    // Concat config to current dev
    InitService.createConfig();
    // Logger, auth...
    InitService.initMiddlewares();

    // init. Global variables, prototypes, connections...
    await InitService.startInit([
        "setGlobals.js",
        "connect.js",
    ]);

    Logger.sysInfo("DEV:", DEV);
    Logger.sysInfo("DEBUG:", DEBUG);
    Logger.sysInfo("start " + APP_NAME);

    const args = InitService.getArgsApp();
    const consoleName = args._[0];
    if (empty(consoleName)) { throw new NotFound("console name not found"); }

    return ConsoleCommandsService.runCommand(consoleName, args);
})().then(exit).catch(exit);
