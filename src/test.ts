import InitService from "./core/classes/services/InitService";
import TestService from "./core/classes/services/TestService";

function exit(err) {
    if (err) { console.error(err.stack); }
    setTimeout(() => process.exit(err ? 1 : 0), 1000);
}

(async () => {
    InitService.initApp("tests");
    // Concat config to current dev
    InitService.createConfig();
    // Logger, auth...
    InitService.initMiddlewares();

    // init. Global variables, prototypes, connections...
    await InitService.startInit([
        "setGlobals.js",
        "knexModules.js",
        "connect.js",
    ]);

    Logger.sysInfo("DEV:", DEV);
    Logger.sysInfo("DEBUG:", DEBUG);
    Logger.sysInfo("start " + APP_NAME);

    const args = InitService.getArgsApp();
    const consoleName = args._[0];
    if (!consoleName) { throw new NotFound("Test name not found"); }

    Logger.info(`Run test ${consoleName}`);
    return TestService.runTest(consoleName);
})().then(exit).catch(exit);
