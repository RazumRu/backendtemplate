export default {
    supportMimetypes: null,
    port: env("UPLOADS_PORT", 3002),
};
