import HttpServerContext from "@core/classes/server/contexts/HttpServerContext";
import ServerCreator from "@core/classes/server/ServerCreator";

export default (async (args) => {
    const serverCreator = ServerCreator.getInstance();
    serverCreator.initAllRoutes();

    const httpServerContext = new HttpServerContext(Config.port, "0.0.0.0");

    await serverCreator.initContexts([httpServerContext]);
});
