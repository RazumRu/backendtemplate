import {UploadService} from "@app/classes/services/UploadService";
import ParentController from "@core/classes/controllers/ParentController";
import {Controller, Middleware, Route} from "@core/classes/decorators/controller";
import {isAdmin, isAuth, isGuest} from "@core/classes/middlewares/auth";
import * as fs from "fs";

@Controller("upload", CONST.ROUTE_TYPE.REST)
@Middleware(isAuth)
export default class UploadController extends ParentController {
    @Route("client")
    public async upload() {
        // @ts-ignore
        const files = $Object(this.getContext().request.files || {}).toArray();
        const result = [];
        const currentPath = Math.round(new Date().getTime() / 1000 / 60 / 60 / 24 / 5);

        for (const file of files) {
            const data = await UploadService.uploadFile("default", {
                name: file.name,
                mimeType: file.mimeType || file.type,
                size: file.size,
                checksum: file.checksum || file.hash,
                data: fs.createReadStream(file.path),
            }, {
                path: "mails/" + currentPath,
            });
            result.push(data);
        }

        return result;
    }
}
