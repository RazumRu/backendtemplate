import ModuleInstance from "@core/classes/apps/ModuleInstance";

export default class Auth extends ModuleInstance {
    constructor(socket) {
        super(socket, __dirname);
    }
}
