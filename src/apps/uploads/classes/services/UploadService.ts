import Path from "@core/classes/helpers/Path";
import fs from "fs";
import mkdirp from "make-dir";
import mime from "mime-types";
import uid from "uid";

export interface File {
    data: any;
    name: string|null;
    mimeType: string;
    size?: number;
    checksum?: string;
}

export interface Options {
    path?: string;
}

export class UploadService {
    private static middlewares = [];

    private static generateFileName() {
        return uid(12);
    }

    private static getMiddleware(index) {
        return this.middlewares[index];
    }

    public static addMiddleware(index, callback) {
        this.middlewares[index] = callback;
    }

    public static validate(file: File) {
        let errorMessage = null;
        if (empty(file.data)) {
            errorMessage = "file empty";
        } else if (empty(file.mimeType)) {
            errorMessage = "mimetype empty";
        } else if (Config.supportMimetypes && !$Array(Config.supportMimetypes).search(file.mimeType)) {
            errorMessage = "mimetype not supported";
        }

        if (errorMessage) { throw new HttpError(500, `File validate error: ${errorMessage}`); }
        return true;
    }

    public static uploadFile(index: string, file: File, options: Options) {
        return (async () => {
            this.validate(file);

            const middleware = this.getMiddleware(index);
            if (!middleware) {
                throw new NotFound(`File upload middleware ${index} not found`);
            }

            return middleware.call(this, file, options);
        })();
    }

    public static async generateFullPath(file: File, path: string = ""): Promise<{uploadDir: string, uploadPath: string, filePath: string}> {
        if (!path.endsWith("\\") || !path.endsWith("/")) { path += "/"; }

        const mimeFile =  "." + mime.extension(file.mimeType);
        let fileName = this.generateFileName();

        const generate = (addRandomString = false) => {
            if (addRandomString) { fileName += Helper.getRandomString(4); }
            const generatedFilename = fileName + mimeFile;

            const filePath = (path + generatedFilename).replace(/^\//i, "");
            const uploadPath = Path.getPath(Config.uploadPath + "/" + filePath);

            return {
                uploadDir: Path.getPath(Config.uploadPath + "/" + path),
                uploadPath,
                filePath,
            };
        };

        const isExist = async (path) => {
            try {
                await $Fn(fs.access).cbPromise(fs, path, fs.constants.F_OK);
                return true;
            } catch (e) {
                return false;
            }
        };

        let pathData = generate();
        let i = 0;
        const maxCount = 30;
        while ((await isExist(pathData.uploadPath)) && i < maxCount) {
            pathData = generate(true);
            i++;
        }

        if (i >= maxCount) {
            throw new HttpError(500, "Number of attempts to generate unique file name exceeded");
        }

        return pathData;
    }

    public static async createDir(uploadDir) {
        const isExists = await new Promise((res) => fs.exists(uploadDir, res));
        if (!isExists) {
            await mkdirp(uploadDir);
        }
    }
}
