import {File, Options, UploadService} from "@app/classes/services/UploadService";
import fs from "fs";

UploadService.addMiddleware("default", async function (file: File, options: Options) {
    const {uploadDir, uploadPath, filePath} = await this.generateFullPath(file, options.path);
    Logger.debug(`upload file ${file.name} to ${uploadPath}`);

    try {
        await this.createDir(uploadDir);
    } catch (e) {
        Logger.error(e);
    }

    await UploadService.validate(file);

    const fileStream = fs.createWriteStream(uploadPath);
    file.data.pipe(fileStream);

    await new Promise((res, rej) =>  {
        file.data.on("error", rej);
        fileStream.once("finish", res);
    });

    const stat: any = await new Promise((res, rej) =>  {
        fs.stat(uploadPath, (err, stat) => {
            if (err) { return rej(err); }
            res(stat);
        });
    });

    return {
        path: filePath,
        filename: file.name,
        size: stat.size,
        checksum: file.checksum,
        mimeType: file.mimeType,
    };
});
