import AppInstance from "@core/classes/apps/AppInstance";
import instance from "./instance";

export default class InboxApp extends AppInstance {
    public async run() {
        this.cluster(1, async (isMaster) => {
            if (!isMaster) { await instance(this.args); }
        });
    }
}
