import Path from "@core/classes/helpers/Path";
import cors from "@koa/cors";
import http from "http";
import Koa from "koa";
import Router from "koa-router";
import KoaStatic from "koa-static";

export default (async (args) => {
    const app = new Koa();

    app.use(KoaStatic(Path.getPath(Config.filesPath)));
    app.use(cors());

    const server = http.createServer(app.callback());
    server.listen(Config.port);

    Events.addEvent("exit", () => {
        server.close();
    });

    Logger.info("upload server start from", Config.port);
});
