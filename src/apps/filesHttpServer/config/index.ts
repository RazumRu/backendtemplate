export default {
    port: env("FILES_HTTP_SERVER_PORT", 8003),
    filesPath: env("UPLOAD_FILES_PATH", "@root/../uploads"),
};
