import path from "./path";

export default {
    path,

    trainMode: false,
    bagOfWords: true,
    minTwoWords: true,

    vectorSize: 300,
    minPointsSearchCommand: 1,

    outputVectorSize: 100,

    trainConfig: {
        rate: 0.04,
        iterations: 15000,
        error: 0.001,
    },

    vectors: {
        // Макс. длинна пропуска между словами
        window: 5,
        // Пороговое значение для появления слов
        sample: 1e-3,
        // использовать иерархический Softmax
        hs: 0,
        // Кол-во отрицательных примеров
        negative: 5,
        // Количество потоков
        iter: 15,
        // Мин. число слов для включения в вектор
        minCount: 1,
        // 0 - skip-gram
        cbow: 0,
    },
};
