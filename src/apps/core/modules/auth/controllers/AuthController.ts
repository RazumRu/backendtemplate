import ParentController from "@core/classes/controllers/ParentController";
import FirebaseFactory from "@core/classes/factory/FirebaseFactory";
import AuthService from "@core/classes/services/AuthService";
import { Controller, Route } from "@root/src/core/classes/decorators/controller";
import { Validate } from "@root/src/core/classes/decorators/validate";

@Controller("auth", CONST.ROUTE_TYPE.REST, [CONST.CONTEXT_TYPE.REST])
export default class AuthController extends ParentController {
    @Route()
    @Validate({
        login: String,
        pass: String,
    })
    public async login() {
        const userServiceInstance = this.getUserService();
        const authService = new AuthService(this.data, this.getContext());

        const user = await authService.check("default");
        const tokenData = await userServiceInstance.generateTokensForUser(
            user._id,
            {
                id: user._id,
                name: user.name,
            },
            60 * 60 * 24,
        );

        Logger.debug({ token: tokenData.token });
        return { token: tokenData.token };
    }
}
