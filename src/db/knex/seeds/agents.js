
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('agent').del()
    .then(function () {
        // Inserts seed entries
        return knex('agent').insert([
        {
            id: 1,
            name: 'testAgent',
            login: 'testAgent',
            password: 'b4c73123479918e504602b1b2012e6d83285da2e', // 1111 or 1234
        },

        {
          id: 2,
          name: 'testAgent2',
          login: 'testAgent2',
          password: 'b4c73123479918e504602b1b2012e6d83285da2e', // 1111 or 1234
        },
        ]);
    }).then(function () {
        return knex.raw(`select setval('agent_id_seq', max(id)) from agent`);
    });
};
