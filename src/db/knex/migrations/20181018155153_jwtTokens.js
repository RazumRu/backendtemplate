
exports.up = function(knex, Promise) {
    return knex.schema.createTable('jwt_tokens', (table) => {
        table.increments();
        table.integer('agent_id').notNull();
        table.string('refresh_token').notNull();

        table.timestamp('created').defaultTo(knex.fn.now());
        table.timestamp('updated').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('jwt_tokens');
};
