
exports.up = function(knex, Promise) {
    return knex.schema.createTable('agent', (table) => {
        table.increments();
        table.string('name').notNull();
        table.string('login').notNull().unique();
        table.string('password').notNull();
        table.timestamp('created').defaultTo(knex.fn.now()).notNull();
        table.timestamp('updated').defaultTo(knex.fn.now()).notNull();
        table.timestamp('last_seen');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('agent');
};
