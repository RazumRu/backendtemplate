
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('audit', (table) => {
            table.increments();
            table.string('action_index').notNull();
            table.integer('object_id').notNull();
            table.json('data');
            table.text('description');

            table.timestamp('created').defaultTo(knex.fn.now());
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('audit');
};
