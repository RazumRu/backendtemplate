
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('contact', (table) => {
            table.increments();
            table.string('email').notNull().unique();
            table.string('first_name').notNull();
            table.string('last_name');

            table.timestamp('created').defaultTo(knex.fn.now());
            table.timestamp('updated').defaultTo(knex.fn.now());
        })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('contact');
};
