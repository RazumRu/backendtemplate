
exports.up = function(knex, Promise) {
    return knex.schema.createTable('mail_state', (table) => {
        table.increments();
        table.integer('mail_id').notNull();
        table.integer('agent_id').notNull();
        table.integer('state_idx').notNull();

        table.index(['mail_id', 'agent_id', 'state_idx']);

        table.timestamp('created').defaultTo(knex.fn.now()).notNull();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('mail_state');
};
