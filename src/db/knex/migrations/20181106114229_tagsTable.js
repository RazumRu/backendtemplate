
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('tag', (table) => {
            table.increments();
            table.string('name').notNull();
            table.integer('type').notNull();
            table.integer('inbox_id').notNull();
            table.boolean('active').notNull().defaultTo(true);

            table.index(['name', 'inbox_id']);

            table.timestamp('created').defaultTo(knex.fn.now()).notNull();
            table.timestamp('updated').defaultTo(knex.fn.now()).notNull();
        })
        .createTable('mail_tag', (table) => {
            table.increments();
            table.integer('tag_id').notNull();
            table.integer('mail_id').notNull();

            table.index(['tag_id', 'mail_id']);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('tag')
        .dropTable('mail_tag')
};
