
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('inbox', (table) => {
            table.increments();
            table.string('name').notNull();
            table.string('email').notNull();
            table.json('auth').notNull();
            table.boolean('active').notNull().defaultTo(true);
            table.integer('type_idx').notNull();

            table.timestamp('created').defaultTo(knex.fn.now()).notNull();
            table.timestamp('updated').defaultTo(knex.fn.now()).notNull();
        })
        .createTable('conversation', (table) => {
            table.increments();
            table.text('subject').notNull();
            table.boolean('solved').defaultTo(false).notNull();
            table.integer('inbox_id').notNull();
            table.specificType('contacts', 'int[]').notNull();

            table.timestamp('created').defaultTo(knex.fn.now()).notNull();
            table.timestamp('updated').defaultTo(knex.fn.now()).notNull();
        })
        .createTable('conversation_state', (table) => {
            table.increments();
            table.integer('conversation_id').notNull();
            table.integer('agent_id').notNull();
            table.integer('status').notNull();
        })
        .createTable('mail', (table) => {
            table.increments();
            table.integer('uid').notNull();
            table.string('mailbox_path');
            // table.index(['inbox_id', 'uid', 'mailbox_type']);
            table.integer('agent_id').notNull();
            table.text('title').notNull();
            table.integer('from').notNull();
            table.specificType('to', 'int[]').notNull();
            table.specificType('cc', 'int[]').notNull();
            table.specificType('bcc', 'int[]').notNull();

            table.boolean('archive').notNull().defaultTo(false);
            table.boolean('outgoing').notNull();
            table.boolean('spam').notNull().defaultTo(false);


            //for reply
            table.string('message_id').notNull();
            table.text('body').notNull();
            table.string('reply_to');
            table.specificType('attachments', 'int[]').notNull();
            table.specificType('references', 'varchar(255)[]');
            table.integer('inbox_id').notNull();
            table.integer('conversation_id').notNull();
           // table.string('contact_id').notNull().references('id').inTable('contacts');
            table.timestamp('date').notNull();

            table.index(["message_id", "inbox_id"]);

            table.timestamp('created').defaultTo(knex.fn.now()).notNull();
            table.timestamp('updated').defaultTo(knex.fn.now()).notNull();
        })
        .createTable('attachments', (table) => {
            table.increments();
            table.string('name').notNull();
            table.string('path').notNull();
            table.string('checksum').notNull();
            table.string('mime_type').notNull();
            table.integer('size').notNull();

            table.timestamp('created').defaultTo(knex.fn.now()).notNull();
            table.timestamp('updated').defaultTo(knex.fn.now()).notNull();
        })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('mail')
        .dropTable('conversation')
        .dropTable('conversation_state')
        .dropTable('inbox')
        .dropTable('attachments');
};
