import Db from "@core/classes/db/Db";

Db.addTypeMongoose(function Phone(val) {
    if (empty(val)) {
        return val;
    }

    if (!Helper.isString(val)) {
        throw new Error(Lang.get("validate.types.string", {name: "phone"}, "Телефон должен быть строкой"));
    }

    if (!/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/.test(val)) {
        throw new Error(Lang.get("validate.types.phone", {name: "phone"}, "Неверный формат телефона"));
    }

    return val;
});
