import Db from "@core/classes/db/Db";

Db.addTypeMongoose(function Email(val) {
    if (empty(val)) {
        return val;
    }

    if (!Helper.isString(val)) {
        throw new Error(Lang.get("validate.types.string", {name: "email"}, "Почта должна быть строкой"));
    }

    if (!/^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i.test(val)) {
        throw new Error(Lang.get("validate.types.email", {name: "email"}, "Почта должна быть заполнена в формате mymail@domain.ru"));
    }

    return val;
});
