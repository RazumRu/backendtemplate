import MongooseModel from "@core/classes/db/MongooseModel";

/**
 * Логи событий
 * @constructor
 */
export default class Action extends MongooseModel {
    constructor() {
        super();

        this.setSchemaName("Лог события");
        this.setNotFoundMessage("Лог события не найден");
        this.searchMethods();

        this.addField("user", {
            type: this.getType("Number"),
        });
        this.addField("type", {
            type: String,
            required: "Не передан тип",
        });
        this.addField("message", {
            type: String,
            required: "Не передано сообщение",
        });
        this.addField("source", {
            type: String,
            required: "Не передан источник",
        });
        this.addField("sourceId", {
            type: this.getType("ObjectId"),
        });
        this.addField("data", {
            type: Object,
        });
    }
}
