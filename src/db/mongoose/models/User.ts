import MongooseModel from "@core/classes/db/MongooseModel";

/**
 * Пользователь
 * @constructor
 */
export default class User extends MongooseModel {
    constructor() {
        super();

        this.setSchemaName("Пользователь");
        this.setNotFoundMessage("Пользователь не найден");
        this.searchMethods();

        this.addField("type", {
            type: Number,
            enum: $Object(CONST.USER_TYPE).toArray(),
            required: "Не передан тип",
        });
        this.addField("name", {
            type: String,
            required: "Не передано имя",
        });
        this.addField("login", {
            type: String,
            required: "Не передан логин",
        });
        this.addField("pass", {
            type: String,
            required: "Не передан пароль",
            hideJSON: true,
        });
        this.addField("chatId", {
            type: String,
        });
        this.addField("options", {
            type: this.getType("Mixed"),
            default: {},
        });
        this.addField("tokensMobile", {
            type: [String],
            default: [],
        });
    }

    public getCommandOption(index: string, key: string) {
        return this.options && this.options[index] && this.options[index][key];
    }

    public setCommandOption(index: string, key: string, value: any) {
        if (!this.options) {
            this.options = {};
        }

        if (!this.options[index]) {
            this.options[index] = {};
        }

        this.options[index][key] = value;
        this.markModified("options");
    }
}
