import MongooseModel from "@core/classes/db/MongooseModel";

/**
 * Различные опции
 * @constructor
 */
export default class Option extends MongooseModel {
    constructor() {
        super();
        this.searchMethods();

        this.setSchemaName("Опция");
        this.setNotFoundMessage("Опция не найдена");
        this.cache(60 * 60 * 24);

        this.addField("name", {
            type: String,
            required: "Не передано имя опции",
            unique: "Такая опция уже существует",
        });

        this.addField("value", {
            type: this.getType("Mixed"),
        });
    }

    /**
     * Получает опцию по имени
     * @param name
     */
    public async getByName(name) {
        const opt = await Db.getMongoModel("Options").findOne({
            name,
        }, {value: 1}).lean().cache();

        return opt && opt.value;
    }

    /**
     * Изменяет или создает опцию
     * @param name
     * @param value
     */
    public async change(name, value) {
        let opt = await Db.getMongoModel("Options").findOne({name}).exec();

        if (opt) {
            opt.value = value;
        } else {
            opt = new (Db.getMongoModel("Options"))({
                name,
                value,
            });
        }
        opt.markModified("value");

        return opt.save();
    }
}
