import MongoosePlugin from "@core/classes/db/MongoosePlugin";
import mongoose from "mongoose";

/**
 * Кеширование в редис. Срабатывает только если у схемы назначена опция redisCache. Можно назначать опцию expires, иначе будет 60
 * @constructor
 * @param schema
 * @param options
 */
export default class MongooseRedisCache extends MongoosePlugin {
    /**
     * Преобразует объект к модели
     * @param model
     * @param fields
     * @param data
     * @returns {*}
     */
    public static getModelForData(this: any, model, fields, data) {
        if (!Helper.isObject(data)) {
            return data;
        }

        const newModel = new model(undefined, fields, true);

        newModel.init(data);

        return newModel;
    }

    /**
     * Получает ключ для кеширования модели
     * @param modelName
     * @returns {string}
     */
    public static getKey(this: any, modelName) {
        return Config.connections.db.mongoose.host + "_mongo_" + modelName;
    }

    /**
     * Очистка кеша модели
     * @param name
     */
    public static async deleteCache(this: any, name) {
        const keyModel = MongooseRedisCache.getKey(name);
        const clearCacheCallbacks = this.schema.options.clearCacheCallbacks || [];

        for (const cb of clearCacheCallbacks) {
            await cb();
        }

        await RedisClient.delete(keyModel);
    }

    /**
     * Вместо exec с кешированием
     */
    public static async cache(this: any) {
        const name = this._collection.collectionName;
        const options = this.options;
        const mongooseOptions = this._mongooseOptions;
        const _query = this._conditions || {};
        const method = this.op;
        const _fields = this._fields || {};
        const lean = this._mongooseOptions.lean;

        const schemaOptions = this.model.schema.options;
        const expires = schemaOptions.expires || 60 * 15;

        if (!Config.cache.cacheMongo || !schemaOptions.redisCache) {
            return mongoose.Query.prototype.exec.apply(this, arguments);
        }

        // Сортируем ключи поиска
        const sortQuery = Object.keys(_query).sort();
        const query = {};
        for (const key of sortQuery) {
            query[key] = _query[key];
        }

        const sortFields = Object.keys(_fields).sort();

        const concat = {
            query,
            options,
            mongooseOptions,
            fields: sortFields,
        };

        const keyValue = "hash_" + $String(name + $Object(concat).toJson() + method).getHash();
        const keyModel = MongooseRedisCache.getKey(name);

        let val = await RedisClient.hmget(keyModel, keyValue, true);

        if (!empty(val)) {
            if (!lean) {
                // Преобразуем в модель
                if (Helper.isArray(val)) {
                    for (const i in val) {
                        val[i] = MongooseRedisCache.getModelForData(this.model, _fields, val[i]);
                    }
                } else if (Helper.isObject(val)) {
                    val = MongooseRedisCache.getModelForData(this.model, _fields, val);
                }
            }

            return val;
        } else {
            const res = await mongoose.Query.prototype.exec.call(this);
            const data = {};

            if (!empty(res)) {
                if (Helper.isObject(res)) {
                    data[keyValue] = res.toObject ? $Object(res.toObject()).toJson() : $Object(res).toJson();
                } else if (Helper.isArray(res)) {
                    const list = [];
                    for (const i in res) { list[i] = res.toObject ? $Object(res.toObject()).toJson() : $Object(res).toJson(); }
                    data[keyValue] = list;
                } else {
                    data[keyValue] = res;
                }
                await RedisClient.hmset(keyModel, data, expires);

                return res;
            } else {
                return res;
            }
        }
    }

    public static run(schema: any, options: any) {
        schema.methods.deleteCache = function () {
            return MongooseRedisCache.deleteCache.call(this, this.collection.collectionName);
        };
        schema.query.cache = MongooseRedisCache.cache;

        // сброс кеша
        schema.pre("save", async function () {
            const options = this.schema.options;

            if (options && Config.cache.cacheMongo && options.redisCache && this.isModified()) {
                Logger.debug("Сброс кеша pre save");
                await this.deleteCache();
            }
        });

        // schema.statics.remove автоматически вызывает данный метод
        schema.query.remove = function (filter, callback) {
            const options = this.schema.options;
            if (typeof filter === "function") {
                callback = filter;
                filter = null;
            }

            const promise = new Promise((res, rej) => {
                mongoose.Query.prototype.remove.call(this, filter, (err, result) => {
                    if (err) { return callback(err); }
                    result = result.result;

                    if (result && result.n > 0) {
                        Logger.debug("Сброс кеша schema.query.remove");
                        MongooseRedisCache.deleteCache.call(this, this.mongooseCollection.name).then(res).catch(rej);
                    } else {
                        res();
                    }
                });
            });

            if (Config.cache.cacheMongo && options.redisCache) {
               if (callback) {
                   promise.then((res) => callback(null, res)).catch(callback);
               } else {
                   return promise;
               }
            } else {
                return mongoose.Query.prototype.remove.call(this, filter, callback);
            }
        };

        schema.pre("remove", async function () {
            const options = this.schema.options;

            if (Config.cache.cacheMongo && options.redisCache) {
                Logger.debug("Сброс кеша pre remove");
                await this.deleteCache();
            }
        });

        // сброс кеша
        schema.pre("update", async function () {
            const options = this.schema.options;

            if (options && Config.cache.cacheMongo && options.redisCache) {
                Logger.debug("Сброс кеша pre update");
                await MongooseRedisCache.deleteCache.call(this, this.mongooseCollection.name);
            }
        });
    }
}
