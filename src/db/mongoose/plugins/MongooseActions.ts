import MongoosePlugin from "@core/classes/db/MongoosePlugin";
import ActionsHistoryService from "@core/classes/services/ActionsHistoryService";

/**
 * Запись событий по изменению в базу
 */
export default class MongooseActions extends MongoosePlugin {
    public static addAction(req, fields) {
        Object.defineProperty(this, "addActionData", {
            value: {
                req,
                fields,
                modified: false,
            },
            writable: true,
        });
        return this;
    }

    /**
     * Вызов обработчика добавления события
     * @param type
     */
    public static callAddingAction(this: any, type) {
        const actionData = this.addActionData;
        if (!actionData) { return; }

        const name = this.collection.name;
        let data = this.clone();
        let initialData = this._initialData && $Object(this._initialData).cloneJson();

        if (actionData.fields) {
            data = $Object(data).filter(actionData.fields);
            if (initialData) { initialData = $Object(initialData).filter(actionData.fields); }
        }

        setImmediate(() => {
            if (type === "remove") {
                ActionsHistoryService.action(type, actionData.req, {
                    data: {
                        initialData,
                        newData: data,
                    },
                    modelName: name,
                    sourceId: this._id,
                });
            } else {
                ActionsHistoryService.action(type, actionData.req, {
                    data: {
                        initialData: initialData && $Object(initialData).filter(actionData.modifiedPaths),
                        newData: $Object(data).filter(actionData.modifiedPaths),
                    },
                    modelName: name,
                    sourceId: this._id,
                });
            }
        });
    }

    /**
     * Вызов обработчика добавления события на поисковые запросы
     * @param type
     */
    public static callQueryAddingAction(this: any, type) {
        const actionData = this.addActionData;
        if (!actionData) { return; }

        const name = this.mongooseCollection.name;
        let data = this._update || {};
        $Object(data).setDefaultProps(data.$set);
        delete data.$set;

        if (actionData.fields) {
            data = $Object(data).filter(actionData.fields);
        }

        setImmediate(() => {
            ActionsHistoryService.action(type, actionData.req, {
                data: {
                    newData: data,
                },
                modelName: name,
                sourceId: data._id,
            });
        });
    }

    public static run(schema: any, options: any) {
        schema.post("init", (doc) => {
            Object.defineProperty(doc, "_initialData", {
                value: doc.toObject(),
                writable: true,
            });
        });

        schema.post("remove", (doc) => {
            if (doc.addActionData) {
                MongooseActions.callAddingAction.call(doc, "remove");
            }
        });

        schema.pre("save", function (callback) {
            if (this.isModified() && this.addActionData) {
                this.addActionData.modified = true;
                this.addActionData.isNew = this.isNew;
                this.addActionData.modifiedPaths = this.modifiedPaths();
            }
            callback();
        });

        schema.post("save", function (doc) {
            if (doc.addActionData && doc.addActionData.modified) {
                if (!doc.addActionData.isNew) {
                    MongooseActions.callAddingAction.call(doc, "update");
                } else {
                    MongooseActions.callAddingAction.call(doc, "insert");
                }
            }

            Object.defineProperty(doc, "_initialData", {
                value: doc.clone(),
                writable: true,
            });
        });

        schema.post("update", function () {
            if (this.addActionData) {
                MongooseActions.callQueryAddingAction.call(this, "update");
            }
        });

        schema.methods.addAction = MongooseActions.addAction;
        schema.query.addAction = MongooseActions.addAction;
    }
}
