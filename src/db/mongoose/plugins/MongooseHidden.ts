import MongoosePlugin from "@core/classes/db/MongoosePlugin";

// Использовать осторожно, тк при lean запросе свойство все равно будет показываться
// @ts-ignore
import mongooseHiddenPlugin from "mongoose-hidden";
const mongooseHidden = mongooseHiddenPlugin({ defaultHidden: { __v: true } });

/**
 * Скрытие полей
 */
export default class MongooseHidden extends MongoosePlugin {
    /**
     * Прячет свойства, отмеченные как hide
     */
    public static hideProps(this: any) {
        const fields = this.schema.obj;
        const select = [];

        for (const name in fields) {
            const field = fields[name];
            if (Helper.isObject(field) && (field.hide || field.hideJSON || field.hideObject)) {
                select.push("-" + name);
            }
        }

        if (select.length) {
            this.select(select.join(" "));
        }

        return this;
    }

    public static run(schema: any, options: any) {
        const virtuals = schema.virtuals;

        const initPluginVirtuals = {};
        for (const index in virtuals) {
            if (!virtuals.hasOwnProperty(index)) { continue; }

            const val = virtuals[index];
            if (val.options.hideJSON && val.options.hideObject) { return initPluginVirtuals[index] = "hide"; }
            if (val.options.hideJSON) { return initPluginVirtuals[index] = "hideJSON"; }
            if (val.options.hideObject) { return initPluginVirtuals[index] = "hideObject"; }
        }

        schema.plugin(mongooseHidden, {virtuals: initPluginVirtuals});
        schema.query.hideProps = MongooseHidden.hideProps;
    }
}
