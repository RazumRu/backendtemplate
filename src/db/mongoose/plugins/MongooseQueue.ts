import MongoosePlugin from "@core/classes/db/MongoosePlugin";
import RequestsQueue from "@core/classes/RequestsQueue";
import async from "async";
import mongoose from "mongoose";
import MongooseHelpers from "./MongooseHelpers";

/**
 * Группировка запросов
 */
export default class MongooseQueue extends MongoosePlugin {
    public static disableQueue(this: any) {
        Object.defineProperty(this, "disableQueue", {
            value: true,
            writable: true,
        });
        return this;
    }

    /**
     * Генерирует ключ для сохранения очереди
     * @param queryObject
     * @returns {string}
     */
    public static generateKey(this: any, queryObject) {
        const name = queryObject._collection.collectionName;
        const options = queryObject.options;
        const mongooseOptions = queryObject._mongooseOptions;
        const _query = queryObject._conditions || {};
        const method = queryObject.op;
        const _fields = queryObject._fields || {};

        // Сортируем ключи поиска
        const sortQuery = Object.keys(_query).sort();
        const query = {};

        for (const key of sortQuery) {
            query[key] = _query[key];
        }

        const sortFields = Object.keys(_fields).sort();

        const concat = {
            query,
            options,
            mongooseOptions,
            fields: sortFields,
        };

        return "hash_" + $String(name + $Object(concat).toJson()).getHash();
    }

    /**
     * Получает копию документа
     * @param model
     * @param fields
     * @param doc
     * @returns {*}
     */
    public static async getCopyDoc(this: any, model, fields, doc) {
        const copy = new model(undefined, fields, true);

        copy.init(MongooseHelpers.clone(doc));
        return copy;
    }

    public static initQueue(this: any): Promise<any> {
        return new Promise((resolve, reject) => {
            const lean = this._mongooseOptions.lean;
            const method = this.op;
            if (!this.disableQueue || method === "update" || (Config.cache.useMongoQueueOnlyLean && !lean)) {
                return mongoose.Query.prototype.exec.call(this).then(resolve).catch(reject);
            }

            const key = MongooseQueue.generateKey(this);
            const initQueue = !RequestsQueue.emptyQueue(key);
            const fields = this._fields || {};

            // Тут немного замысловато. На каждый возвращемый объект нужно сделать его КОПИЮ, иначе получится так, что несколько запросов будет обслуживать один и тот же объект
            RequestsQueue.addQueue(key, async function (err, data) {
                if (err) { return reject(err); }
                // let countQueue = RequestsQueue.getCountQueue(key);
                if (!data || lean) {
                    resolve(data && MongooseHelpers.clone(data));
                    return;
                }

                if (!Helper.isArray(data)) {
                    MongooseQueue.getCopyDoc(this.model, fields, data).then(resolve).catch(reject);
                    return;
                } else {
                    const result = [];
                    await new Promise((resolve, reject) => {
                        async.eachOfLimit(data, 5, (doc, index, cb) => {
                            MongooseQueue.getCopyDoc(this.model, fields, doc).then((cloneDoc) => {
                                result[index] = cloneDoc;
                                cb();
                            }).catch(cb);
                        }, (err) => {
                            if (err) { return reject(err); }
                            resolve();
                        });
                    });
                    resolve(result);
                }
            });

            if (!initQueue) {
                this.lean();
                mongoose.Query.prototype.exec.call(this).then((data) => {
                    RequestsQueue.callQueue(key, this, null, data);
                }).catch((err) => {
                    RequestsQueue.callQueue(key, this, err, null);
                });
            }
        });
    }

    public static run(schema: any, options: any) {
        if (Config.cache.useMongoQueue) { schema.query.exec = MongooseQueue.initQueue; }
        schema.query.disableQueue = MongooseQueue.disableQueue;
    }
}
