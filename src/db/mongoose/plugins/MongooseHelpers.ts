import MongoosePlugin from "@core/classes/db/MongoosePlugin";
import mongoose from "mongoose";

export default class MongooseHelpers extends MongoosePlugin {
    /**
     * Заполение данными
     * @param data
     */
    public static fill(this: any, data) {
        const fields = Object.keys(this.schema.obj);

        for (const i in data) {
            if ($Array(fields).search(i) && !$Array(["_id", "_v"]).search(i)) {
                this[i] = data[i];
            }
        }
    }

    /**
     * Валидирует и проверяет на существование по уникальным полям
     * @param callback
     * @returns {*}
     */
    public static async validateData(this: any, callback) {
        const fields = this.schema.obj;

        const or = [];
        const fieldsUnique = [];
        for (const index in fields) {
            const field = fields[index];

            if (field.unique) {
                or.push({[index]: this[index]});
                fieldsUnique[index] = 1;
            }
        }

        const err = this.validateSync();
        if (err) {
            throw new HttpError(400, "Validation error", err);
        }

        if (!or.length) {
            return true;
        }

        const elem = await Db.getMongoModel(this.modelName)
            .findOne({
                _id: {
                    $ne: this._id,
                },
            }, Object.assign({}, fieldsUnique, {_id: 1}))
            .or(or)
            .lean();

        if (!elem) {
            return true;
        }

        const errors = [];
        for (const index in fields) {
            const field = fields[index];
            if (field.unique && this[index].toString() === elem[index].toString()) {
                errors.push(field.unique);
            }

        }

        throw new HttpError(500, "Ошибка проверки на уникальность: " + errors.join(", "));
    }

    /**
     * Клонирует данные модели
     * @returns {*}
     */
    public static clone(this: any, clone) {
        let newObject = {};
        if (Helper.isArray(clone)) { newObject = []; }

        (function _clone(clone, currentObject) {
            if (Helper.isArray(clone) && !clone.length) {
                return currentObject;
            }

            const $clone = clone.toObject ? clone.toObject() : clone;

            for (const i in $clone) {
                if (!$clone.hasOwnProperty(i)) {
                    continue;
                }

                const value = $clone[i];

                if (Helper.isString(value) || Helper.isNumber(value) || Helper.isFunc(value) || Helper.isBool(value)) {
                    currentObject[i] = value;
                    continue;
                }
                if (value instanceof Date) {
                    currentObject[i] = new Date(value.getTime());
                    continue;
                }
                if (value instanceof Db.connectionMongoose.Types.ObjectId) {
                    currentObject[i] = value;
                    continue;
                }

                if (Helper.isArray(value)) {
                    currentObject[i] = _clone(value, []);
                    continue;
                }

                if (Helper.isObject(value)) {
                    currentObject[i] = _clone(value, {});
                    continue;
                }
            }

            return currentObject;
        })(clone, newObject);

        // return clone.$cloneJson();
        return newObject;
    }

    /**
     * Должен обязательно существовать
     */
    public static exist(this: any) {
        Object.defineProperty(this, "_isExist", {
            value: true,
            writable: true,
        });

        return this;
    }

    public static run(schema: mongoose.Schema, options: any) {
        // Автоматическое изменение поля
        schema.pre("save", async function (this: any)  {
            if (this.isModified()) {
                this.modified = new Date();
            }
        });

        schema.methods.fill = MongooseHelpers.fill;
        schema.methods.validateData = MongooseHelpers.validateData;
        schema.methods.clone = function (this: any) {
            let clone = this._doc;
            if (!clone) {
                clone = this.toObject();
            }

            return MongooseHelpers.clone(clone);
        };

        schema.query.exist = MongooseHelpers.exist;

        schema.post("findOne", function (this: any, doc, next) {
            if (this._isExist && !doc) {
                const schemaOptions = this.model.schema.options;
                const notFoundMessage = schemaOptions.notFoundMessage || "элемент не найден";
                return next(new NotFound(notFoundMessage));
            }
            next();
        });
    }
}
