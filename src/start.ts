import InitService from "./core/classes/services/InitService";

function exit(err) {
    if (err) { console.error(err.stack); }
    setTimeout(() => process.exit(err ? 1 : 0), 1000);
}

(async () => {
    InitService.initApp();
    // Concat config to current dev
    InitService.createConfig();
    // Logger, auth...
    InitService.initMiddlewares();

    // init. Global variables, prototypes, connections...
    await InitService.startInit([
        "setGlobals.js",
        "connect.js",
    ]);

    Logger.sysInfo("DEV:", DEV);
    Logger.sysInfo("DEBUG:", DEBUG);
    Logger.sysInfo("start " + APP_NAME);

    let app = require(`@app`);
    app = app.default || app;
    if (!app) { throw new NotFound(`App ${APP_NAME} not found`); }
    const instanceApp = new app(InitService.getArgsApp());
    if (!instanceApp.run) { throw new NotFound(`Missing run method`); }

    let exitStatus = false;
    process.on("exit", () => {
        if (!exitStatus) {
            exitStatus = true;
            Events.emitInstance("exit");
        }
    });

    process.on("SIGTERM", () => {
        if (!exitStatus) {
            exitStatus = true;
            Events.emitInstance("exit");
        }
    });

    return instanceApp.run();
})().catch(exit);
