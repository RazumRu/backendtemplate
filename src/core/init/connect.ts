import Path from "@core/classes/helpers/Path";
import fs from "fs";
import requireAll from "require-all";
import RedisClient from "../classes/clients/RedisClient";
import Db from "../classes/db/Db";
import InitService from "../classes/services/InitService";

export default async () => {
    // db
    {
        await Db.connectMongo();

        const typesPath = Path.getPath("@root/src/db/mongoose/types");
        if (fs.existsSync(typesPath)) { requireAll(typesPath); }

        const modelsPath = Path.getPath("@root/src/db/mongoose/models");
        Db.addModelsMongo(modelsPath);

        await Db.insureIndex();

        InitService.addGlobal("Db", Db);
    }

    const redisClient = RedisClient.getInstance();
    await redisClient.connect(Config.connections.redis);
    InitService.addGlobal("RedisClient", redisClient);

};
