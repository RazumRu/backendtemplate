/// <reference path="../config/globalsDeclare/globals.d.ts" />

import Helper from "@core/classes/helpers/Helper";
import Lang from "@core/classes/helpers/Lang";
import Logger from "@core/classes/helpers/Logger";
import InitService from "@core/classes/services/InitService";

import HttpError from "@core/classes/errors/HttpError";
import NotFound from "@core/classes/errors/NotFound";
import ParentError from "@core/classes/errors/ParentError";
import SysError from "@core/classes/errors/SysError";
import Events from "@core/classes/Events";
import $Array from "@core/classes/helpers/$Array";
import $Date from "@core/classes/helpers/$Date";
import $Fn from "@core/classes/helpers/$Fn";
import $Number from "@core/classes/helpers/$Number";
import $Object from "@core/classes/helpers/$Object";
import $String from "@core/classes/helpers/$String";
import _ from "lodash";

// set globals...
InitService.addGlobal("_", _);
InitService.addGlobal("$String", (str: string | number) => new $String(str));
InitService.addGlobal("$Object", (object: any) => new $Object(object));
InitService.addGlobal("$Number", (numb: string | number) => new $Number(numb));
InitService.addGlobal("$Date", (date: string | Date | number) => new $Date(date));
InitService.addGlobal("$Array", (array?: Array<any>) => new $Array(array));
InitService.addGlobal("$Fn", (body?: any) => new $Fn(body));

InitService.addGlobal("Helper", Helper);
InitService.addGlobal("empty", Helper.empty.bind(Helper));
InitService.addGlobal("Logger", Logger);
InitService.addGlobal("Events", Events);

InitService.addGlobal("ParentError", ParentError);
InitService.addGlobal("HttpError", HttpError);
InitService.addGlobal("SysError", SysError);
InitService.addGlobal("NotFound", NotFound);

InitService.addGlobal("DEBUG", Config.debug);
InitService.addGlobal("CONST", Config.const);
InitService.addGlobal("Lang", Lang);
