import Validator from "../../classes/helpers/Validator";

Validator.addCustomType("time", "The {name} field must be filled in the format HH:MM", (value) => {
    return /^\d\d:\d\d$/.test(value);
});

Validator.addCustomType("email", "The {name} field should be filled in the format mymail@domain.ru", (value) => {
    return /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i.test(value);
});

Validator.addCustomType("phone", "The {name} field should be filled as a phone", (value) => {
    return /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/.test(value);
});
