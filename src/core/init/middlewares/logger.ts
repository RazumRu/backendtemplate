import Logger from "@core/classes/helpers/Logger";

Logger.addMiddleware("info", function (...arg) {
    this.print({
        color: "green",
        background: null,
        isError: false,
        tpl: "{date} - [info {pid}] {data}",
    }, ...arg);
});

// redis
Logger.addMiddleware("redis", function (...arg) {
    if (DEBUG) {
        this.print({
            color: "black",
            background: "bgMagenta",
            isError: false,
            tpl: "{date} - [redis {pid}] {data}",
        }, ...arg);
    }
});

Logger.addMiddleware("request", function (...arg) {
    if (DEBUG) {
        this.print({
            color: "cyan",
            background: null,
            isError: false,
            tpl: "{date} - [request {pid}] {data}",
        }, ...arg);
    }
});

// tasks
Logger.addMiddleware("tasks", function (...arg) {
    this.print({
        color: "magenta",
        isError: false,
        tpl: "{date} - [tasks {pid}] {data}",
    }, ...arg);
});

// sysInfo
Logger.addMiddleware("sysInfo", function (...arg) {
    this.print({
        color: "black",
        background: "bgWhite",
        isError: false,
        tpl: "{date} - [sysinfo {pid}] {data}",
    }, ...arg);
});

// warn
Logger.addMiddleware("warn", function (...arg) {
    this.print({
        color: "yellow",
        background: null,
        isError: false,
        isWarn: true,
        tpl: "{date} - [warn {pid}] {data}",
    }, ...arg);
});

// debug
Logger.addMiddleware("debug", function (...arg) {
    if (DEBUG) {
        this.print({
            color: "black",
            background: "bgCyan",
            isError: false,
            tpl: "{date} - [debug {pid}] {data}",
        }, ...arg);
    }
});

Logger.addMiddleware("debugColor", function (printColor: string|Array<string>, ...arg) {
    if (DEBUG) {
        let color, background = null;

        if (Helper.isArray(printColor)) {
            color = printColor[0];
            background = printColor[1];
        } else {
            color = printColor;
        }

        this.print({
            color: color || "black",
            background,
            isError: false,
            tpl: "{date} - [debug {pid}] {data}",
        }, ...arg);
    }
});

Logger.addMiddleware("inbox", function (...arg) {
    if (DEBUG) {
        this.print({
            color: "cyan",
            background: null,
            isError: false,
            tpl: "{date} - [debug {pid} inbox] {data}",
        }, ...arg);
    }
});

Logger.addMiddleware("db", function (...arg) {
    if (DEBUG) {
        this.print({
            color: "magenta",
            background: null,
            isError: false,
            tpl: "{date} - [debug {pid} db] {data}",
        }, ...arg);
    }
});

// error
Logger.addMiddleware("error", function (err) {
    let logString = err;
    if (Helper.isObject(err) && !(err instanceof Error)) {
        logString = $Object(err).toJson();
    }

    this.print({
        color: "red",
        background: null,
        isError: true,
        tpl: "{date} - [error {pid}] {data}",
    }, logString);
});
