import AuthService from "@core/classes/services/AuthService";
import UserService from "@core/classes/services/UserService";

AuthService.addStrategy("default", async  (data, context) => {
    if (!Helper.isObject(data)) {
        throw new HttpError(500, "No data transferred");
    }

    if (Helper.empty(data.pass)) {
        throw new NotFound("Pass not found");
    }

    if (Helper.empty(data.login)) {
        throw new NotFound("Login not found");
    }

    const login = data.login.toString();

    const user = await Db.getMongoModel("User").findOne({login}).exist();

    if (UserService.encryptPass(data.pass) !== user.pass) {
        throw new HttpError(403, "Password is incorrect");
    }

    return user;
});
