import ConsoleInstance from "@core/classes/apps/ConsoleInstance";
import InitService from "@core/classes/services/InitService";

/**
 * run migrations rollback
 */
export default class MigrateRollback extends ConsoleInstance {
    public async run() {
        const dir = InitService.replacePathAlias(this.args.dir || "@root/src/db/migrations");
        Logger.info("start migrate rollback from " + dir);

        await Db.getKnex().migrate.rollback({
            directory: dir,
        });
    }
}
