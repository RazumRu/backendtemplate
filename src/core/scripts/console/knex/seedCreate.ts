import ConsoleInstance from "@core/classes/apps/ConsoleInstance";
import InitService from "@core/classes/services/InitService";

/**
 * generate seed
 */
export default class SeedCreate extends ConsoleInstance {
    public async run() {
        const dir = InitService.replacePathAlias(this.args.dir || "@projectRoot/src/db/seeds");
        Logger.info("start generate seed to " + dir);
        const seedName = this.args.name;
        if (!seedName) { throw new NotFound("Not found seed name"); }

        await Db.getKnex().seed.make(seedName, {
            directory: dir,
        });
    }
}
