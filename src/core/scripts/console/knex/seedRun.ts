import ConsoleInstance from "@core/classes/apps/ConsoleInstance";
import InitService from "@core/classes/services/InitService";
import fs from "fs";

/**
 * run seed
 */
export default class SeedRun extends ConsoleInstance {
    public async run() {
        const dir = InitService.replacePathAlias(this.args.dir || "@root/src/db/seeds");
        const environmentDir = InitService.replacePathAlias(this.args.dir || "@root/src/db/seeds/" + (DEV ? "dev" : "prod"));

        Logger.info("run seed from " + dir, environmentDir);

        await Db.getKnex().seed.run({
            directory: dir,
        });

        if (fs.existsSync(environmentDir)) {
            await Db.getKnex().seed.run({
                directory: environmentDir,
            });
        }
    }
}
