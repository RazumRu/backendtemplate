import ConsoleInstance from "@core/classes/apps/ConsoleInstance";
import ConsoleCommandsService from "@core/classes/services/ConsoleCommandsService";
import InitService from "@core/classes/services/InitService";
import knexTables from "knex-cleaner/lib/knex_tables";

/**
 * run migrations
 */
export default class Migrate extends ConsoleInstance {
    public async run() {
        const dir = InitService.replacePathAlias(this.args.dir || "@root/src/db/migrations");
        Logger.info("start migrate from " + dir);

        const force = Helper.default(this.args.force, false);
        if (force) {
            Logger.warn("force mode");
            const tables = await knexTables.getTableNames(Db.getKnex());

            let i = 0;
            while (tables.length) {
                const res = await Db.getKnex().schema.dropTable(tables[i]).catch((err) => {
                    Logger.warn(err);
                    return null;
                });

                if (res) {
                    tables.splice(i, 1);
                } else {
                    i++;
                }
                if (i >= tables.length - 1) { i = 0; }
            }
        }

        Logger.info("migrate... ");
        await Db.getKnex().migrate.latest({
            directory: dir,
        });

        const runSeed = Helper.default(this.args.seed, false);
        if (runSeed) {
            Logger.debug("run seeds...");
            await ConsoleCommandsService.runCommand("seedRun", {
                dev: this.args.dev,
            });
        }

        Logger.info("success");
    }
}
