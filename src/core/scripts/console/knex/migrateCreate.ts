import ConsoleInstance from "@core/classes/apps/ConsoleInstance";
import InitService from "@core/classes/services/InitService";

/**
 * generate migration
 */
export default class MigrateCreate extends ConsoleInstance {
    public async run() {
        const dir = InitService.replacePathAlias(this.args.dir || "@projectRoot/src/db/migrations");
        Logger.info("start generate migrate to " + dir);
        const migrationName = this.args.name;
        if (!migrationName) { throw new NotFound("Not found migration name"); }

        await Db.getKnex().migrate.make(migrationName, {
            directory: dir,
        });
    }
}
