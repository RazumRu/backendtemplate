import {assert} from "chai";
import chaiHttp from "chai-http";

describe("$Array", () => {
    describe("merge", () => {
        it("check mergeStrings correct data", () => {
            const arr = [1, "2", "test"];
            const arr2 = [4];
            const target = [1, "3", "4", "test", "test2"];
            const assignArray = $Array(target).mergeStrings(arr, arr2);

            assert.deepEqual(assignArray, ["1", "3", "4", "test", "test2", "2"], "not correct data");
            assert.deepEqual(target, [1, "3", "4", "test", "test2"], "target changed");
        });

        it("check merge correct data", () => {
            const arr = [1, "2", "test"];
            const arr2 = [4];
            const target = [1, "3", "4", "test", "test2"];
            const assignArray = $Array(target).merge(arr, arr2);

            assert.deepEqual(assignArray, [1, "3", "4", "test", "test2", "2", 4], "not correct data");
            assert.deepEqual(target, [1, "3", "4", "test", "test2"], "target changed");
        });
    });

    describe("search", () => {
        it("check search elements", () => {
            const arr = [1, "2", "test"];

            assert.equal($Array(arr).search([1, "2"]), true, "not correct search");
            assert.equal($Array(arr).search([1, "2", 2]), false, "not correct search");
        });
    });

    describe("removeVal", () => {
        it("check removeVal", () => {
            const arr = [1, "2", "test"];

            assert.deepEqual($Array(arr).removeVal([1, "2"]), ["test"], "not correct remove");
        });
    });

    describe("removeDuplicateValues", () => {
        it("check removeDuplicateValues", () => {
            const arr = [1, 1, "1"];

            assert.deepEqual($Array(arr).removeDuplicateValues(), [1, "1"], "not correct");
            assert.deepEqual($Array(arr).removeDuplicateValues(true), ["1"], "not correct strings");
        });
    });
});

describe("$Object", () => {
    describe("filter", () => {
        it("check correct filter", () => {
            const obj = {
                a: 1,
                b: {
                    a: [1, "2", [3]],
                    b: "2",
                },
                c: {
                    a: {
                        a: [{h: 8}],
                    },
                },
                d: "7",
            };
            assert.deepEqual($Object(obj).filter(["a"]), {a: 1}, "not correct filter");
            assert.deepEqual($Object(obj).filter("a d"), {a: 1, d: "7"}, "not correct filter");
            assert.deepEqual($Object(obj).filter((key, val) => val === 1), {a: 1}, "not correct filter");
        });
    });

    describe("merge", () => {
        it("check correct mergeDeep", () => {
            const obj = {
                a: 1,
                b: [1, 2],
                c: {d: {k: 5}},
            };

            assert.deepEqual($Object(obj).mergeDeep({a: 5, b: [3], c: {d: {k: 8, l: 5}}, t: 8}, {h: 54}), {
                a: 1,
                b: [1, 2, 3],
                c: {d: {k: 5, l: 5}},
                t: 8,
                h: 54,
            }, "not correct mergeDeep");
        });

        it("check correct setDefaultProps", () => {
            const obj: any = {
                a: 1,
                b: [1, 2],
                i: 1,
            };
            $Object(obj).setDefaultProps({a: 5, b: [3], c: {g: 4}}, {h: 54});

            assert.deepEqual(obj, {
                a: 1,
                b: [1, 2],
                c: {g: 4},
                h: 54,
                i: 1,
            }, "not correct setDefaultProps");
        });
    });

    describe("clone", () => {
        it("check correct clone", () => {
            const obj = {
                a: 1,
                b: {
                    a: [1, "2", [3]],
                    b: "2",
                },
                c: {
                    a: {
                        a: [{h: 8}],
                    },
                },
            };
            const clone = $Object(obj).cloneDeep();

            obj.a = 9;
            assert.notEqual(obj.a, clone.a);

            obj.b.b = "9";
            assert.notEqual(obj.b.b, clone.b.b);

            obj.b.a[0] = 9;
            assert.notEqual(obj.b.a[0], clone.b.a[0]);

            obj.b.a[2][0] = 9;
            assert.notEqual(obj.b.a[2][0], clone.b.a[2][0]);

            obj.c.a.a[0].h = 9;
            assert.notEqual(obj.c.a.a[0].h, clone.c.a.a[0].h);
        });
    });
});
