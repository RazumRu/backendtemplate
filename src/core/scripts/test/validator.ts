import Validator from "@core/classes/helpers/Validator";
import {assert} from "chai";
import chaiHttp from "chai-http";

describe("Validator", () => {
    function getValidator() {
        return new Validator({
            number: 5,
            _number: "10.6",
            bool: false,
            _bool: 1,
            string: "test",
            date: new Date().toString(),
            _date: new Date(),
            array: [1, "ff"],
            object: {
                f: 5,
            },
            time: "19:22",
        });
    }

    it("Object", () => {
        const isValid = getValidator().validate({
            object: Object,
        });

        const _isValid = getValidator().validate({
            string: Object,
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });

    it("Number", () => {
        const isValid = getValidator().validate({
            number: Number,
            _number: Number,
        });

        const _isValid = getValidator().validate({
            string: Number,
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });

    it("Boolean", () => {
        const isValid = getValidator().validate({
            bool: Boolean,
            _bool: Boolean,
            number: Boolean,
        });

        const _isValid = getValidator().validate({
            _number: Boolean,
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });

    it("Date", () => {
        const isValid = getValidator().validate({
            date: Date,
            _date: Date,
        });

        const _isValid = getValidator().validate({
            string: Date,
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });

    it("String", () => {
        const isValid = getValidator().validate({
            string: String,
        });

        const _isValid = getValidator().validate({
            number: String,
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });

    it("Array", () => {
        const isValid = getValidator().validate({
            array: Array,
        });

        const _isValid = getValidator().validate({
            number: Array,
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });

    it("custom", () => {
        const isValid = getValidator().validate({
            time: "time",
        });

        const _isValid = getValidator().validate({
            number: "time",
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });

    it("exist", () => {
        const isValid = getValidator().validate({
            "time": "time",
            "data?": Number,
        });

        const _isValid = getValidator().validate({
            time: "time",
            data: Number,
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });

    it("{type}", () => {
        const isValid = getValidator().validate({
            number: {
                type: Number,
                name: "test name",
            },
        });

        const _isValid = getValidator().validate({
            number: {
                type: String,
                name: "test name",
            },
        });

        assert.equal(isValid, true);
        assert.equal(_isValid, false);
    });
});
