/// <reference path="../../../../node_modules/@types/lodash/index.d.ts" />

declare class Helper {
    public static declOfNum(count: number, titles: Array<string>): string;

    public static empty(value): boolean;
    public static env(paramName: string, defaultValue: string|number): string|number|null;
    public static default(value, def): any;
    public static getRandomString(length: number): string;
    public static random(min: number, max: number): number;

    public static isObject(value): boolean;
    public static isArray(value): boolean;
    public static isFunc(value): boolean;
    public static isBool(value): boolean;
    public static isString(value): boolean;
    public static isNumber(value): boolean;
    public static isDate(value): boolean;
    public static isBuffer(value): boolean;

    public static base64_encode(text: string): string;
    public static base64_decode(text: string): string;
}

declare function empty(value: any): boolean;
declare function env(paramName: string, defaultValue: string|number): string|number|null;

declare class Db {
    public static connectionMongoose: any;

    public static connect(): void;
    public static getMongo(): any;
    public static getKnex(): any;
    public static getMongoModels(): any;
    public static getMongoModel(name: string): any;
}

declare class Logger {
    public static info(...args): void;
    public static redis(...args): void;
    public static request(...args): void;
    public static tasks(...args): void;
    public static sysInfo(...args): void;
    public static warn(...args): void;
    public static debug(...args): void;
    public static debugColor(color: string|Array<string>, ...args): void;
    public static inbox(...args): void;
    public static db(...args): void;
    public static error(err: Error|string): void;
}

declare class Events {
    public static getInstance(...args): void;
    public static addEvent(index: string, cb): void;
    public static emitInstance(...args): void;
    public emit(event: string|symbol, ...args: Array<any>): boolean;
}

declare class ParentError extends Error {
    public readonly prototype: Error;
    public number: number;
    public code: number;
    public lineNumber: number;
    public fileName: string;
    public parentError: Error;
    constructor(code: number, message?: string, err?: any)
}

declare class HttpError extends ParentError {
    constructor(code: number, message?: string, err?: any)
}

declare class SysError extends ParentError {
    constructor(message: string, err?: any)
}

declare class NotFound extends ParentError {
    constructor(message: string, err?: any)
}

declare class RedisClient {
    public static hmset(key: string, data: any, expires: number);
    public static set(key: string, value: string|number, expires: number);
    public static get(key: string, json?: boolean);
    public static hmget(key: string, valueKey: string|Array<string>, json?: boolean);
    public static hvals(key: string);
    public static hkeys(key: string);
    public static hgetall(key: string);
    public static delete(key: string);
    public static hdel(key: string, keys: string|Array<string>);
}

declare class StringHelper extends String {
    public getHash(): number;
    public toString(): string;
    public translit(engToRus?: boolean): StringHelper;
    public punto(engToRus?: boolean): StringHelper;
    public crop(size: number): StringHelper;
    public stripTags(): StringHelper;
    public format(data: any): StringHelper;
    public escape(): StringHelper;
    public escapeTags(): StringHelper;
    public replaceNewLines(): StringHelper;
    public toNumber(): number;
    public toUrl(url?: string, https?: boolean): string;
    public ofJson(): any;
}

declare const $String: (str: string | number) => StringHelper;

declare class ObjectHelper extends Object {
    public toQuery(): string;
    public mapValues(callback: (value: any) => any): any;
    public cloneDeep(): any;
    public mergeDeep(...objects: any): any;
    public toJson(): string;
    public getObject(): any;
    public cloneJson(): any;
    public getLength(): number;
    public toArray(): Array<any>;
    public getProp(prop: string): any;
    public setDefaultProps(...args: any): any;
    public filter(property: ((index: string | number, value: any) => boolean) | string | Array<string>): any;
}

declare const $Object: (object: any) => ObjectHelper;

declare class NumberHelper extends Number {
    public show(valute?: string, fixed?: number, nbsp?: boolean): string;
    public secondsToString(showSec?: boolean): string;
}

declare const $Number: (numb: string | number) => NumberHelper;

declare class DateHelper extends Date {
    public show(tpl?: string | ((date: Date) => string), offsetMinutes?: number): string;
    public isValid(): boolean;
}

declare const $Date: (date: string | Date | number) => DateHelper;

declare class ArrayHelper extends Array {
    public merge(...arrays: Array<any>): Array<any>;
    public mergeStrings(...arrays: Array<any>): Array<any>;
    public search(values: Array<string | number> | string | number): boolean;
    public searchByString(values: Array<string | number> | string | number): boolean;
    public removeVal(value: Array<any> | string | number): Array<any>;
    public toObject(callback: (val: any) => Array<any>): any;
    public getRandom(): any;
    public removeDuplicateValues(convertToString?: boolean): Array<any>;
}

declare const $Array: (array?: Array<any>) => ArrayHelper;

declare class FnHelper extends Function {
    public cbPromise(context: any, ...arg): Promise<any>;
}

declare const $Fn: (body?: any) => FnHelper;

declare var Config;
declare var Lang;
declare var DEBUG;
declare var CONST;
declare var APP_NAME;
declare var DEV;
declare var _: _.LoDashStatic;
