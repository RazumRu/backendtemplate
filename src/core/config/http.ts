export default {
    maxFieldsSize: 20  *  1024  *  1024, // 20mb
    maxFileSize: 200  *  1024  *  1024, // 200mb
};
