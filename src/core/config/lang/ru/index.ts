export default {
    system: {
        processRun: "Process {pid} running",
        processComplete: "Process {pid} completed with code: {code}, message: {message}",
        emptyInstanceName: "Instance not passed",
    },

    errors: {
        notFoundDefaultMessage: "Not Found",
        httpErrorDefaultMessage: "Http Error",
        sysErrorDefaultMessage: "System Error",
    },

    validate: {
        success: "Verification successful",
        emptyData: "No data transferred: {list}",
        emptyFieldData: "One of the fields is not passed: {list}",

        types: {
            number: "The {name} field must be a number",
            array: "The {name} field must be a array",
            string: "The {name} field must be a string",
            object: "The {name} field must be a object",
            boolean: "The {name} field must be a boolean",
            date: "The {name} field must be the date",
            regular: "The {name} field filled incorrectly",
            regularMany: "Values contain invalid characters: {list}",
            notEqual: "Values {a} and {b} are not equal",
        },
    },

    auth: {
        strategyNotFound: "Authorization strategy not found",
    },

    router: {
        controllerNotFound: "Controller {name} not found",
        methodNotFound: "Method {name} not found",
        cbNotFound: "Callback not found",
        accessDenied: "Access is denied",
    },
};
