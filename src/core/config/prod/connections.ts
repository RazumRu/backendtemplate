export default {
    uploadServer: env("UPLOAD_SERVER_URL", "http://files.rrjarvis.ru/"),

    db: {
        knex: {
            client: env("KNEX_CLIENT", "pg"),

            connection: {
                host : env("KNEX_HOST", "localhost"),
                user : env("KNEX_USER", "inbox"),
                port : env("KNEX_PORT", 5432),
                password : env("KNEX_PASS", "J8BNsO1eNRUvJmzEeOgm"),
                database : env("KNEX_DB", "inbox"),
            },
        },

        mongoose: {
            host : env("MONGO_HOST", "localhost"),
            user : env("MONGO_USER", "razumru"),
            port : env("MONGO_PORT", 27017),
            password : env("MONGO_PASS", "05253545"),
            database : env("MONGO_DB", "jarvis"),
        },
    },

    redis: {
        host: env("REDIS_HOST", "127.0.0.1"),
        port: env("REDIS_PORT", 6379),
        password: env("REDIS_PASS", undefined),
    },
};
