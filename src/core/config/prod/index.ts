import connections from "./connections";

export default {
    debug: parseInt(String(env("DEBUG", 1)), 10),
    connections,

    uploadPath: env("UPLOAD_FILES_PATH", "/upload"),
};
