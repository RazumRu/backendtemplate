export default {
    // Additional layer between radishes. Saves the radish cache for a certain number of seconds in order not to make frequent repeated requests to it
    // It works only with a single instance! Very neat to use
    localCacheRedis: false,
    // Use queues for redis requests (grouping requests).
    useRedisQueue: true,
    // Every n seconds, the local radish cache will be cleared.
    cacheRedisSeconds: 10,
    // Maximum string size that can be stored in local cache in bytes
    cacheRedisMaxLength: 100000, // Примерно 0,2Мб

    // Pack size of callbacks in line
    queueGroupSize: 10,

    // Кеширование запросов монго
    cacheMongo: true,

    // Использовать очереди для запросов монго(группировка запросов). Спорно для не lean запросов,
    useMongoQueue: true,
    useMongoQueueOnlyLean: true,
};
