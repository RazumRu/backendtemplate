import cache from "./cache";
import connections from "./connections";
import constants from "./const";
import http from "./http";
import lang from "./lang/ru";
import path from "./path";
import services from "./services";

export default {
    debug: true,
    secretKey: "tR5jpEwnHG80Bf3Db6",

    const: constants,
    connections,
    lang,
    cache,
    path,
    http,
    services,
};
