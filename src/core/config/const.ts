export default {
    CONTEXT_TYPE: {
        SOCKETS: 1,
        REST: 2,
        TELEGRAM: 3,
    },

    USER_TYPE: {
        ADMIN: 1,
    },

    ROUTE_TYPE: {
        REST: "rest",
        GET: "get",
        POST: "post",
        REGEXP: "regexp",
        STRING: "string",
        CALLBACK: "callback",
        VOICE: "voice",
        MESSAGE: "message",
    },

    COMMAND_CONTEXT: {
        TELEGRAM: 1,
        REST: 2,
    },
};
