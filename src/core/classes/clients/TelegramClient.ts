import MessageFactory from "@core/classes/telegram/factory/MessageFactory";
import Message from "@core/classes/telegram/Message";
import TelegramBot from "node-telegram-bot-api";
import Agent from "socks5-https-client/lib/Agent";

type Proxy = {
    host: string,
    port: number,
    user?: string,
    pass?: string,
};

type MessageCallback = (message: Message) => Promise<void>;

export default class TelegramClient {
    private onCheckout: MessageCallback;
    private onMessage: MessageCallback;
    private onVoice: MessageCallback;
    private onSuccessfulPayment: MessageCallback;
    // Бинды на сообщение
    private binds: Array<{callback: MessageCallback, value: string | RegExp}> = [];
    // Одноразовые бинды для чата
    private chatBinds = new Map<number, Array<MessageCallback>>();
    private proxy: Proxy;
    private callbacksQuery = new Map<string, MessageCallback>();

    private errorHandler(err: Error) {
        Logger.error(err);
    }
    public connection: TelegramBot;

    public setProxy(proxy: Proxy) {
        this.proxy = proxy;
    }

    public getProxyRequestOptions() {
        if (!this.proxy) { return {}; }

        return {
            agentClass: Agent,
            agentOptions: {
                socksHost: this.proxy.host,
                socksPort: this.proxy.port,
                socksUsername: this.proxy.user,
                socksPassword: this.proxy.pass,
            },
        };
    }

    public getCallbackQuery(index: string): MessageCallback {
        return this.callbacksQuery.get(index);
    }

    /**
     * Соединение
     * @param token
     */
    public connect(token: string) {
        const options: any = {
            polling: true,
        };
        options.request = this.getProxyRequestOptions();

        this.connection = new TelegramBot(token, options);
        Logger.info("connect telegram", token, options);

        this.connection.on("error", console.log);
        this.connection.on("polling_error", console.log);
        this.connection.on("webhook_error", console.log);

        // Каллбек на проверку оплаты
        this.connection.on("pre_checkout_query", (msg) => {
            if (!this.onCheckout) { return; }
            const message = MessageFactory.createMessage(msg);

            this.removeChatBind(message.chatId);
            this.onCheckout(message).catch(this.errorHandler);
        });

        // Каллбек на успешную онлайн оплату
        this.connection.on("successful_payment", (msg) => {
            if (!this.onSuccessfulPayment) { return; }
            const message = MessageFactory.createMessage(msg);

            this.removeChatBind(message.chatId);
            this.onSuccessfulPayment(message).catch(this.errorHandler);
        });

        // Каллбек на любое сообщение
        this.connection.on("message", (msg) => {
            Logger.debug("message", msg);
            const message = MessageFactory.createMessage(msg);

            // Голосовое сообщение
            if (msg.voice && this.onVoice) {
                this.removeChatBind(message.chatId);
                return this.onVoice(message).catch(this.errorHandler);
            }

            // Прочие бинды
            for (const i in this.binds) {
                const bind = this.binds[i];
                const value = bind.value;

                if (message.getText()) {
                    const text = message.getText();

                    const isStringTrue = Helper.isString(value) && value === text;
                    const isRegExpTrue = (value instanceof RegExp) && value.test(text);
                    if (isStringTrue || isRegExpTrue) {
                        this.removeChatBind(message.chatId);
                        return bind.callback(message).catch(this.errorHandler);
                    }
                }
            }

            // Разовые бинды
            if (!this.callChatBind(message.chatId, message)) {
                if (message.getText() && this.onMessage) {
                    this.removeChatBind(message.chatId);
                    return this.onMessage(message).catch(this.errorHandler);
                }
            }
        });

        // Каллбек на клик по кнопке
        this.connection.on("callback_query", (req) => {
            if (req.data) {
                const message = MessageFactory.createMessage(req.message, req.id);

                this.removeChatBind(message.chatId);

                const callbackQuery = this.callbacksQuery.get(req.data);
                if (callbackQuery) {
                    callbackQuery(message).then(() => {
                        return this.connection.answerCallbackQuery(req.id);
                    }).catch(this.errorHandler);
                }
            }
        });
    }

    /**
     * Устанавливает бинд на запись
     * @param callback
     */
    public setOnVoice(callback: MessageCallback) {
        this.onVoice = callback;
    }

    /**
     * Устанавливает бинд на текст
     * @param callback
     */
    public setOnMessage(callback: MessageCallback) {
        this.onMessage = callback;
    }

    /**
     * Устанавливает бинд на оплату
     * @param callback
     */
    public setOnCheckout(callback: MessageCallback) {
        this.onCheckout = callback;
    }

    /**
     * Устанавливает бинд на оплату
     * @param callback
     */
    public setOnSuccessfulPayment(callback: MessageCallback) {
        this.onSuccessfulPayment = callback;
    }

    /**
     * Выполняет первый разоый биндд чата
     * @param chatId
     * @param message
     */
    public callChatBind(chatId: number, message: Message) {
        const chatBinds = this.chatBinds.get(chatId);

        if (chatBinds && chatBinds.length) {
            const cb = chatBinds.shift();
            if (cb) {
                cb(message).catch(this.errorHandler);
            }
            this.chatBinds.set(chatId, chatBinds);

            return true;
        }
        return false;
    }

    /**
     * Удаляет очередь разовых биндов чата
     * @param chatId
     */
    public removeChatBind(chatId: number) {
        this.chatBinds.delete(chatId);
    }

    /**
     * Отправка сообщения
     * @param message
     * @param options
     * @returns {Promise}
     */
    public sendMessage(message: Message, options: TelegramBot.SendMessageOptions = {}) {
        $Object(options).setDefaultProps(message.getDataFromApi());
        Logger.debug("send",  message.getText(), options);
        return this.connection.sendMessage(message.chatId, message.getText(), options);
    }

    /**
     * Отправка сообщения
     * @param message
     * @param options
     * @returns {Promise}
     */
    public sendPhoto(message: Message, options: TelegramBot.SendPhotoOptions = {}) {
        $Object(options).setDefaultProps(message.getDataFromApi());
        Logger.debug("sendPhoto",  message.getPhoto(), options);
        return this.connection.sendPhoto(message.chatId, message.getPhoto(), options);
    }

    /**
     * Отправка счета
     * @returns {Promise}
     * @param message
     * @param options
     */
    public sendInvoice(message: Message, options: TelegramBot.SendInvoiceOptions = {}) {
        $Object(options).setDefaultProps(message.getDataFromApi());
        const invoice = message.getInvoice();

        Logger.debug("sendInvoice", invoice, options = {});
        return this.connection.sendInvoice(message.chatId, invoice.title, invoice.description, invoice.payload, invoice.providerToken, invoice.startParameter, invoice.currency, invoice.prices, options);
    }

    /**
     * Отправка локации
     * @param message
     * @param options
     * @returns {Promise}
     */
    public sendLocation(message: Message, options: TelegramBot.SendLocationOptions = {}) {
        $Object(options).setDefaultProps(message.getDataFromApi());
        Logger.debug("sendLocation",  message.getLocation(), options);
        return this.connection.sendLocation(message.chatId, message.getLocation()[0], message.getLocation()[1], options);
    }

    /**
     * Редактирование сообщения
     * @param message
     * @param options
     * @returns {Promise}
     */
    public editMessageText(message: Message, options: TelegramBot.EditMessageTextOptions = {}) {
        $Object(options).setDefaultProps(message.getDataFromApi());
        if (options.reply_markup) {
            options.reply_markup = $Object(options.reply_markup).filter("inline_keyboard");
        }

        const sendData = {
            message_id: parseInt(message.messageId, 10),
            chat_id: message.chatId,
            ...options,
        };
        Logger.debug("editMessageText", message.getText(), sendData);
        return this.connection.editMessageText(message.getText(), sendData);
    }

    /**
     * Редактирование сообщения
     * @param message
     * @param options
     * @returns {Promise}
     */
    public editMessageCaption(message: Message, options: TelegramBot.EditMessageCaptionOptions = {}) {
        $Object(options).setDefaultProps(message.getDataFromApi());
        return this.connection.editMessageCaption(message.getCaption(), {
            message_id: parseInt(message.messageId, 10),
            chat_id: message.chatId,
            ...options,
        });
    }

    /**
     * Редактирование клавиатуры сообщения
     * @param message
     * @param replyMarkup
     * @returns {Promise}
     */
    public editMessageKeyboard(message: Message, replyMarkup: TelegramBot.InlineKeyboardMarkup) {
        return this.connection.editMessageReplyMarkup(replyMarkup, {
            message_id: parseInt(message.messageId, 10),
            chat_id: message.chatId,
        });
    }

    /**
     * Удаение сообщения
     * @param message
     * @returns {Promise}
     */
    public deleteMessage(message: Message) {
        return this.connection.deleteMessage(message.chatId, message.messageId);
    }

    /**
     * обавляет бинд
     * @param value
     * @param callback
     */
    public addBind(value, callback: MessageCallback) {
        this.binds[value.toString()] = {
            value,
            callback,
        };
    }

    /**
     * Добавляет обработчик на нажатие кнопки
     * @param index
     * @param callback
     */
    public addCallback(index, callback: MessageCallback) {
        this.callbacksQuery.set(index, callback);
    }

    /**
     * Добавляет разовый бинд
     * @param chatId
     * @param callback
     */
    public addChatBind(chatId, callback: MessageCallback) {
        let chatBinds = this.chatBinds.get(chatId);
        if (!chatBinds) { chatBinds = []; }

        chatBinds.push(callback);
        this.chatBinds.set(chatId, chatBinds);
    }
}
