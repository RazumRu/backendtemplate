import {EventEmitter} from "events";
import Inbox from "inbox";
import {simpleParser} from "mailparser";

/**
 * Protocol for interacting with mailboxes
 */
export default class ImapClient {
    private readonly user: string;
    private readonly password: string;
    private readonly agent: {host: string, port: number};

    private connection;
    private currentMailbox;

    constructor(options: {user: string, password: string, agent: {host: string, port: number}}) {
        this.user = options.user;
        this.password = options.password;
        this.agent = options.agent;
    }

    public openMailbox(path = "INBOX"): Promise<{name: string, path: string, type?: string, hasChildren?: boolean, delimiter?: string}> {
        return new Promise((res, rej) => {
            if (this.currentMailbox && this.currentMailbox.path === path) {
                return res(this.currentMailbox);
            }

            this.connection.openMailbox(path, (error, info) => {
                if (error) { return rej(error); }
                this.currentMailbox = info;
                Logger.inbox("Open mailbox", path);
                res(info);
            });
        });
    }

    public async getMailboxList(list?: Array<any>): Promise<any> {
        let allMailboxes = [];

        if (!list) {
            list = await $Fn(this.connection.listMailboxes).cbPromise(this.connection);
        }

        for (const mailbox of list) {
            if (mailbox.hasChildren) {
                allMailboxes = [...allMailboxes, mailbox, ...await $Fn(mailbox.listChildren).cbPromise(mailbox)];
            } else {
                allMailboxes.push(mailbox);
            }
        }

        return allMailboxes;
    }

    public async connect() {
        const agentConfig = this.agent;

        /**
         * For gmail you need:
         * 1 - enable imap access in settings
         * 2 - allow third-party applications in security settings (https://myaccount.google.com/u/2/security?pageId=none&pli=1&nlr=1#)
         * 3 - enable two-stage authentication
         * 4 - create a password for the application and use it
         */
        this.connection = Inbox.createConnection(agentConfig.port, agentConfig.host, {
            secureConnection: true,
            auth: {
                user: this.user,
                pass: this.password,
            },
        });

        this.connection.connect();

        await new Promise((res, rej) => {
            this.connection.on("error", rej);
            this.connection.on("connect", res);
        });

        Logger.info("connected to imap", agentConfig);

        return this.connection;
    }

    public async disconnect() {
        this.connection.close();
    }

    public getMessageByUID(UID) {
        return new Promise((res, rej) => {
            this.connection.fetchData(UID, (error, message) => {
                if (error) { return rej(error); }
                res(message);
            });
        });
    }

    public async createEmitterOnNewMessages(): Promise<EventEmitter> {
        const emitter = new EventEmitter();

        this.connection.on("new", (message) => {
            emitter.emit("message", message);
        });

        return emitter;
    }

    public async getMessagesByUID(startUID: number, endUID: number|string = "*"): Promise<Array<any>> {
        const messagesList: any = await new Promise((res, rej) => {
            this.connection.listMessagesByUID(startUID, endUID, (err, uidList: Array<number>) => {
                if (err) { return rej(err); }
                res(uidList);
            });
        });

        messagesList.sort((a, b) => a.UID > b.UID ? 1 : -1);

        return messagesList;
    }

    /**
     * Feed for more information about the message
     * @param uid
     */
    public getMessageStream(uid) {
        return this.connection.createMessageStream(uid);
    }

    public async parseMessage(UID: number, mailboxPath: string): Promise<any> {
        await this.openMailbox(mailboxPath);

        const stream = this.getMessageStream(UID);
        return simpleParser(stream);
    }
}
