import RequestsQueue from "@core/classes/RequestsQueue";
import {ClientOpts, createClient} from "redis";

export default class RedisClient {
    public static getInstance(): RedisClient {
        if (!this.instance) { this.instance = new RedisClient(); }
        return this.instance;
    }
    private client;
    private tempRedisCache = {};
    private static instance;

    /**
     * Gets the key for the queue
     * @param key
     * @param keyValue
     * @returns {String}
     */
    private generateKey(key, keyValue?: Array<string>) {
        let keyValueVal = "";
        // Сортируем ключи
        if (keyValue) {
            keyValue.sort();
            keyValueVal = keyValue.join("_");
        }

        return "hash_" + $String((key + keyValueVal)).getHash();
    }

    /**
     * Clears temporary radish cache
     */
    private clearTempCache(key?: string|number) {
        if (!Config.cache.localCacheRedis) { return; }

        if (key) {
            delete this.tempRedisCache[key];
        } else {
            this.tempRedisCache = {};
        }
    }

    /**
     * Assigns a temporary radish cache
     * @param key
     * @param value
     */
    private setTempRedisCache(key, value) {
        if (!Config.cache.localCacheRedis) { return; }
        // If the line is too large - do not spend RAM
        if (Helper.isString(value) && value.length > Config.cache.cacheRedisMaxLength) { return; }
        this.tempRedisCache[key] = value;
    }

    /**
     * Receives a temporary radish cache
     * @param key
     */
    private getTempRedisCache(key) {
        if (!Config.cache.localCacheRedis) { return null; }
        const value = this.tempRedisCache[key];
        if (value) { Logger.redis("get local cache redis", key); }
        return value;
    }

    private setDataFormat(data: any): string {
        if (Helper.isObject(data)) { data = $Object(data).toJson(); }
        return data.toString();
    }

    private getDataFormat(data: string, json = true): string {
        let returnData: any = data;
        if (data && json) {
            returnData = $String(data).ofJson();
        }
        return returnData;
    }

    constructor() {
        if (Config.cache.localCacheRedis) {
            setInterval(() => {
                this.clearTempCache();
            }, Config.cache.cacheRedisSeconds * 1000).unref();
        }
    }

    public async connect(connect?: ClientOpts) {
        Logger.sysInfo("redis connect", connect);

        this.client = createClient(connect);
      /*  if (connect && connect.password) {
            return this.client.auth(connect.password);
        }*/
    }

    public hmset(key: string, data: any, expires: number) {
        return new Promise((res, rej) => {
            this.client.hmset(key, data, (err) => {
                Logger.redis("hmset", key, expires);
                if (err) { return rej(err); }

                if (expires) {
                    this.client.expire(key, expires);
                }

                if (Config.cache.localCacheRedis) {
                    const keyQueue = this.generateKey(key);
                    this.clearTempCache(keyQueue);
                }

                res();
            });
        });
    }

    public set(key: string, value: any, expires: number) {
        return new Promise((res, rej) => {
            this.client.set(key, this.setDataFormat(value), (err) => {
                Logger.redis("set", key, expires);
                if (err) { return rej(err); }

                if (expires) {
                    this.client.expire(key, expires);
                }

                if (Config.cache.localCacheRedis) {
                    const keyQueue = this.generateKey(key);
                    this.clearTempCache(keyQueue);
                }

                res();
            });
        });
    }

    public get(key: string, json = false) {
        return new Promise((res, rej) => {
            const keyQueue = this.generateKey(key);
            const cache = this.getTempRedisCache(keyQueue);
            if (cache) { return res(cache); }

            let initQueue;
            if (Config.cache.useRedisQueue) {
                initQueue = !RequestsQueue.emptyQueue(keyQueue);
                RequestsQueue.addQueue(keyQueue, async (err, value) => {
                    if (err) { return rej(err); }
                    res(value);
                });
            }

            if (!initQueue) {
                this.client.get(key, (err, value) => {
                    Logger.redis("get", key);
                    value = this.getDataFormat(value, json);
                    this.setTempRedisCache(keyQueue, value);
                    if (Config.cache.useRedisQueue) {
                        RequestsQueue.callQueue(keyQueue, this, err, value);
                    } else {
                        if (err) { return rej(err); }
                        res(value);
                    }
                });
            }
        });
    }

    /**
     * Get the hash entry from the cache ((defined key) key, valueKey)
     */
    public hmget(key: string, valueKey: string|Array<string>, json = false) {
        return new Promise((res, rej) => {
            let args = [key];
            let valuesSearch = valueKey;
            if (!Helper.isArray(valuesSearch)) {
                // @ts-ignore
                valuesSearch = [valuesSearch];
            }
            if (!valuesSearch.length) { return res(); }

            // @ts-ignore
            const keyQueue = this.generateKey(key, valuesSearch);
            const cache = this.getTempRedisCache(keyQueue);
            if (cache) { return res(cache); }

            let initQueue;
            if (Config.cache.useRedisQueue) {
                initQueue = !RequestsQueue.emptyQueue(keyQueue);
                RequestsQueue.addQueue(keyQueue, async (err, value) => {
                    if (err) { return rej(err); }
                    res(value);
                });
            }

            if (!initQueue) {
                args = args.concat(valuesSearch);
                // @ts-ignore
                args.push((err, value) => {
                    Logger.redis("hmget", key, valueKey);
                    let _val = Helper.isArray(valueKey) ? value : (value && value[0]);
                    _val = this.getDataFormat(_val, json);
                    this.setTempRedisCache(keyQueue, _val);
                    if (Config.cache.useRedisQueue) {
                        RequestsQueue.callQueue(keyQueue, this, err, _val);
                    } else {
                        if (err) { return rej(err); }
                        res(_val);
                    }
                });

                this.client.hmget.apply(this.client, args);
            }

        });
    }

    public hvals(key: string) {
        return new Promise((res, rej) => {
            const keyQueue = this.generateKey(key);
            const cache = this.getTempRedisCache(keyQueue);
            if (cache) { return res(cache); }

            let initQueue;
            if (Config.cache.useRedisQueue) {
                initQueue = !RequestsQueue.emptyQueue(keyQueue);
                RequestsQueue.addQueue(keyQueue, async (err, value) => {
                    if (err) { return rej(err); }
                    res(value);
                });
            }

            if (!initQueue) {
                this.client.hvals(key, (err, vals) => {
                    Logger.redis("hvals", key, vals);
                    this.setTempRedisCache(keyQueue, vals);
                    if (Config.cache.useRedisQueue) {
                        RequestsQueue.callQueue(keyQueue, this, err, vals);
                    } else {
                        if (err) { return rej(err); }
                        res(vals);
                    }
                });
            }
        });
    }

    public hkeys(key: string) {
        return new Promise((res, rej) => {
            const keyQueue = this.generateKey(key);
            const cache = this.getTempRedisCache(keyQueue);
            if (cache) { return res(cache); }

            let initQueue;
            if (Config.cache.useRedisQueue) {
                initQueue = !RequestsQueue.emptyQueue(keyQueue);
                RequestsQueue.addQueue(keyQueue, async (err, value) => {
                    if (err) { return rej(err); }
                    res(value);
                });
            }

            if (!initQueue) {
                this.client.hkeys(key, (err, keys) => {
                    Logger.redis("hkeys", key, keys);
                    this.setTempRedisCache(keyQueue, keys);
                    if (Config.cache.useRedisQueue) {
                        RequestsQueue.callQueue(keyQueue, this, err, keys);
                    } else {
                        if (err) { return rej(err); }
                        res(keys);
                    }
                });
            }
        });
    }

    public hgetall(key: string) {
        return new Promise((res, rej) => {
            const keyQueue = this.generateKey(key);
            const cache = this.getTempRedisCache(keyQueue);
            if (cache) { return res(cache); }

            let initQueue;
            if (Config.cache.useRedisQueue) {
                initQueue = !RequestsQueue.emptyQueue(keyQueue);
                RequestsQueue.addQueue(keyQueue, async (err, value) => {
                    if (err) { return rej(err); }
                    res(value);
                });
            }

            if (!initQueue) {
                this.client.hgetall(key, (err, value) => {
                    Logger.redis("hgetall", key, value);
                    this.setTempRedisCache(keyQueue, value);
                    if (Config.cache.useRedisQueue) {
                        RequestsQueue.callQueue(keyQueue, this, err, value);
                    } else {
                        if (err) { return rej(err); }
                        res(value);
                    }
                });
            }
        });
    }

    public delete(key: string) {
        return new Promise((res, rej) => {
            const keyQueue = this.generateKey(key);

            let initQueue;
            if (Config.cache.useRedisQueue) {
                initQueue = !RequestsQueue.emptyQueue(keyQueue);
                RequestsQueue.addQueue(keyQueue, async (err) => {
                    if (err) { return rej(err); }
                    res();
                });
            }

            if (!initQueue) {
                this.client.del(key, (err) => {
                    Logger.redis("del", key);
                    this.clearTempCache(keyQueue);
                    if (Config.cache.useRedisQueue) {
                        RequestsQueue.callQueue(keyQueue, this, err);
                    } else {
                        if (err) { return rej(err); }
                        res();
                    }
                });
            }
        });
    }

    public hdel(key: string, keys: string|Array<string>) {
        return new Promise((res, rej) => {
            if (!Helper.isArray(keys)) {
                // @ts-ignore
                keys = [keys];
            }
            // @ts-ignore
            const keyQueue = this.generateKey(key, keys);

            let initQueue;
            if (Config.cache.useRedisQueue) {
                initQueue = !RequestsQueue.emptyQueue(keyQueue);
                RequestsQueue.addQueue(keyQueue, async (err) => {
                    if (err) { return rej(err); }
                    res();
                });
            }

            if (!initQueue) {
                this.client.hdel(key, keys, (err) => {
                    Logger.redis("hdel", key, keys);
                    this.clearTempCache(keyQueue);
                    if (Config.cache.useRedisQueue) {
                        RequestsQueue.callQueue(keyQueue, this, err);
                    } else {
                        if (err) { return rej(err); }
                        res();
                    }
                });
            }
        });
    }
}
