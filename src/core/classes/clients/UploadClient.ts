import fs from "fs";
import request from "request-promise";

export default class UploadClient {
    public static async sendFilesToServer(files) {
        files = files.map((file) => {
            return {
                value: file.data,
                options: {
                    filename: file.name,
                    contentType: file.mimeType,
                },
            };
        });

        let data;

        try {
            const url = Config.connections.uploadServer + "upload/server";
            Logger.debug("request load files", url);
            data = await request({
                url,
                method: "POST",
                headers: {
                    "secret-key": Config.secretKey,
                },
                formData: files,
                json: true,
            });
        } catch (e) {
            throw new HttpError(500, "Error loading files at " + Config.connections.uploadServer + "upload", e);
        }

        return data;
    }
}
