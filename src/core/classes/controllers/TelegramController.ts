import UserService from "@core/classes/services/UserService";
import TelegramClient from "../clients/TelegramClient";
import TelegramServerContext from "../server/contexts/TelegramServerContext";
import MessageFactory from "../telegram/factory/MessageFactory";
import Form from "../telegram/Form";
import {Keyboard} from "../telegram/Keyboard";
import Message from "../telegram/Message";

export default class TelegramController {
    protected telegramClient: TelegramClient;
    protected contextInstance: TelegramServerContext;

    protected constructor(private contextData: any, data: any) {
        this.message = contextData.context;
        this.contextInstance = contextData.contextInstance;
        this.telegramClient = data.telegramClient;
    }
    private readonly message: Message;

    public createMessage(chatId?: number) {
        return MessageFactory.createForwardMessage(chatId || this.getMessage().chatId);
    }

    /**
     * Отправка сообщения
     * @param message
     */
    public async sendMessage(message: Message) {
        const update = (msg) => {
            message.clearChangesData();
            message.setMessageInfo(msg);
            message.setUser(msg.from);
        };

        if (message.getText()) {
            const msg = await this.telegramClient.sendMessage(message);
            update(msg);
            return message;
        }

        if (message.getPhoto()) {
            const msg = await this.telegramClient.sendPhoto(message);
            update(msg);
            return message;
        }

        if (message.getLocation()) {
            const msg = await this.telegramClient.sendLocation(message);
            update(msg);
            return message;
        }

        if (message.getInvoice()) {
            const msg = await this.telegramClient.sendInvoice(message);
            update(msg);
            return message;
        }

        if (message.getForm()) {
            const form = message.getForm();
            message.setText(form.text);
            const msg = await this.telegramClient.sendMessage(message);
            update(msg);
            const addValidBind = () => {
                this.addChatBind(async (message) => {
                    try {
                        await form.valid(message.getText());
                        const callback = form.callback;

                        if (callback) {
                            await callback(message);
                        }
                    } catch (e) {
                        await this.contextInstance.handlerError(e, this.contextData);
                        addValidBind();
                    }
                });
            };

            addValidBind();
            return message;
        }
    }

    protected getUserService(): UserService {
        return this.contextData.userService;
    }

    protected async getUserInfo() {
        const userServiceInstance = this.getUserService();
        return await userServiceInstance.getUserInfo();
    }

    protected async getUserInfoOrError() {
        const userServiceInstance = this.getUserService();
        return await userServiceInstance.getUserInfoOrError();
    }

    protected addChatBind(callback) {
        if (Helper.isString(callback)) {
            callback = this.telegramClient.getCallbackQuery(callback);
        }

        if (!callback) { return null; }

        return this.telegramClient.addChatBind(this.message.chatId, callback);
    }

    protected getMessage() {
        return this.message;
    }

    protected createKeyboard(name: string, callback?: string) {
        return new Keyboard(name, callback);
    }

    protected createForm(text: string, callback?: (message: Message) => Promise<void>) {
        return new Form(text, callback);
    }

    protected async editMessage(message?: Message) {
        if (!message) { message = this.message; }
        const changesData = message.changesData;

        if (changesData.text) { await this.telegramClient.editMessageText(message); }
        if (changesData.caption) { await this.telegramClient.editMessageCaption(message); }
    }

    protected deleteMessage(message: Message) {
        return this.telegramClient.deleteMessage(message);
    }

    protected getUserPhoto(message: Message) {
        if (message) { message = this.message; }
        return this.telegramClient.connection.getUserProfilePhotos(message.getUserId()).then((data) => {
            return data.photos[0][0].file_id;
        });
    }

}
