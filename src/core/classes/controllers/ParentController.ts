import {Context, ContextData} from "@core/classes/interfaces/ServerInterfaces";
import UserService from "@core/classes/services/UserService";
import HttpContextController from "./HttpContextController";
import SocketContextController from "./SocketContextController";

export default abstract class ParentController {
    protected constructor(protected contextData, protected data: any) {}

    protected getContext() {
        return this.contextData.context;
    }

    protected getContextName(): string|number {
        return this.contextData.contextName;
    }

    protected checkContextName(name: string|number): boolean {
        return this.getContextName() === name;
    }

    protected getUserService(): UserService {
        return this.contextData.userService;
    }

    protected getContextInstance(): Context {
        return this.contextData.contextInstance;
    }

    protected async getUserInfo() {
        const userServiceInstance = this.getUserService();
        return await userServiceInstance.getUserInfo();
    }

    protected async getUserInfoOrError() {
        const userServiceInstance = this.getUserService();
        return await userServiceInstance.getUserInfoOrError();
    }

    protected getTokenInfo(): any {
        const userServiceInstance = this.getUserService();
        return userServiceInstance.getTokenInfo();
    }

    protected getSocketContextController(): SocketContextController {
        return new SocketContextController(this.contextData);
    }

    protected getHttpContextController(): HttpContextController {
        return new HttpContextController(this.contextData);
    }
}
