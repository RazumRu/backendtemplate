import {Context, ContextData} from "@core/classes/interfaces/ServerInterfaces";
import SocketServer from "@core/classes/server/SocketServer";

export default class SocketContextController {
    protected socketServer: SocketServer;

    constructor(protected contextData: ContextData) {
        this.socketServer = SocketServer.getInstance();
    }

    public getSocket() {
        return this.contextData.context;
    }

    public getContextInstance(): Context {
        return this.contextData.contextInstance;
    }

    public getSocketServer(): SocketServer {
        return this.socketServer;
    }

    /**
     * Sends an event to sockets from a specific group.
     * Sends to each assigned group in turn
     * @param groups
     * @param index
     * @param data
     * @returns {*}
     */
    public emit(groups, index, data = {}) {
        data = this.getContextInstance().formatSendData(data);
        this.socketServer.emit(groups, index, data);
    }

    /**
     * Sends an event to sockets from a specific group.
     * Sends to each assigned group in turn
     * @param groups
     * @param index
     * @param data
     * @returns {*}
     */
    public emitBroadcast(groups, index, data = {}) {
        data = this.getContextInstance().formatSendData(data);
        this.socketServer.emitBroadcast(this.getSocket(), groups, index, data);
    }

    /**
     * send data current socket
     */
    public emitSocket(index, data = {}) {
        data = this.getContextInstance().formatSendData(data);
        Logger.debug("emitSocket", index);
        this.getSocket().emit(index, data);
    }

    public emitSocketError(err: ParentError, data) {
        data = this.getContextInstance().formatSendData({
            code: err.number || err.code || 500,
            message: err.toString(),
            data,
        });

        this.getSocket().emit("err", data);
    }
}
