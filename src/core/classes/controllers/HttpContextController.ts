import {Context, ContextData} from "@core/classes/interfaces/ServerInterfaces";

export default class HttpContextController {
    constructor(protected contextData: ContextData) {}

    public getCtx() {
        return this.contextData.context;
    }

    public getContextInstance(): Context {
        return this.contextData.contextInstance;
    }
}
