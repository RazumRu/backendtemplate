import $Array from "@core/classes/helpers/$Array";
import Helper from "@core/classes/helpers/Helper";

export default class $Object {
    constructor(private object: any) {
    }

    public getObject() {
        return this.object;
    }

    public toQuery(): string {
        const result = [];

        for (const name in this.object) {
            if (!this.object.hasOwnProperty(name)) { continue; }

            const value = this.object[name];
            if (empty(value)) { continue; }

            if (Helper.isObject(value) || Helper.isArray(value)) {
                for (const i in value) {
                    if (!value.hasOwnProperty(i)) { continue; }

                    result.push($String("{name}[{index}]={value}").format({
                        name: encodeURI ? encodeURIComponent(name) : name,
                        index: encodeURI ? encodeURIComponent(i) : name,
                        value: encodeURIComponent(String(value[i])),
                    }));
                }
            } else {
                result.push($String("{name}={value}").format({
                    name: encodeURI ? encodeURIComponent(name) : name,
                    value: encodeURIComponent(String(value)),
                }));
            }
        }

        return result.join("&");
    }

    public cloneDeep(): any {
        return _.cloneDeep(this.object);
    }

    public mapValues(callback: (value: any) => any): any {
        return _.mapValues(this.object, callback);
    }

    public filter(property: ((index: string | number, value: any) => boolean) | string | Array<string>): any {
        const result: any = {};

        if (property) {
            if (Helper.isFunc(property)) {
                for (const i in this.object) {
                    // @ts-ignore
                    if (property(i, this.object[i])) { result[i] = this.object[i]; }
                }
            } else {
                if (Helper.isString(property)) { property = String(property).split(" "); }
                return _.pick(this.object, property as any);
            }
        }

        return result;
    }

    public mergeDeep(...objects: any): any  {
        return _.mergeWith(this.object, ...objects, (objValue, srcValue) => {
            if (Helper.isArray(objValue)) {
                return _.union(objValue, srcValue);
            }

            if (Helper.isObject(objValue)) {
                return new $Object(objValue).mergeDeep(srcValue);
            }

            return objValue;
        });
    }

    public toJson(): string {
        return JSON.stringify(this.object);
    }

    public cloneJson(): any {
        return $String(new $Object(this.object).toJson()).ofJson();
    }

    public getLength(): number {
        return Object.keys(this.object).length;
    }

    public setDefaultProps(...args: any): any {
        return _.assignWith(this.object, ...args, (objValue, srcValue) => {
            return Helper.empty(objValue) ? srcValue : objValue;
        });
    }

    public toArray(): Array<any> {
        return Object.values(this.object);
    }

    public getProp(prop: string): any {
        return _.get(this.object, prop);
    }
}
