export default class $Date extends Date {
    constructor(date: string | Date | number) {
        super(date);
    }

    public show(tpl?: string | ((date: Date) => string), offsetMinutes?: number): string {
        const currentDate = new Date();
        const newDate = this;
        if (offsetMinutes) { newDate.setMinutes(newDate.getMinutes() + offsetMinutes); }

        let day: any = newDate.getDate();
        if (day < 10) { day = "0" + day; }
        let month: any = newDate.getMonth() + 1;
        if (month < 10) { month = "0" + month; }
        const year = newDate.getFullYear();

        let hours = newDate.getHours();
        const normalizeHours = hours;
        let timeShort = "AM";
        if (hours === 12) {
            timeShort = "PM";
        } else if (hours > 12) {
            hours -= 12;
            timeShort = "PM";
        }
        let minutes: any = newDate.getMinutes ();
        if (minutes < 10) { minutes = "0" + minutes; }
        let seconds: any = newDate.getSeconds();
        if (seconds < 10) { seconds = "0" + seconds; }

        const mText = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
        const mTextWhen = ["январе", "феврале", "марте", "апреле", "мае", "июне", "июле", "августе", "сентябре", "октябре", "ноябре", "декабре"];
        const mTextOne = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"];
        const mTextShort = [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ];
        let dText = day;

        if (currentDate.getMonth() === newDate.getMonth() && currentDate.getFullYear() === year) {
            if (parseInt(day, 10) === currentDate.getDate()) { dText = "Сегодня"; }
            if (currentDate.getDate() - parseInt(day, 10) === 1) { dText = "Вчера"; }
            if (currentDate.getDate() - parseInt(day, 10) === -1) { dText = "Завтра"; }
        }

        const daysName = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"];
        const daysNameValue = daysName[newDate.getDay()];

        const minutesOffset = Math.ceil((currentDate.getTime() - newDate.getTime()) / 1000 / 60);
        let agoText = "";
        if (minutesOffset < 60) {
            agoText = `${minutesOffset} минут назад`;
        } else if (minutesOffset < 60 * 24) {
            agoText = `${Math.floor(minutesOffset / 60)} часов назад`;
        } else {
            agoText = `${Math.floor(minutesOffset / 60 / 24)} дней назад`;
        }

        const defaultTpl = "{d}.{m}.{y} {h}:{min}:{s}";

        if (tpl && typeof tpl === "function") {
            tpl = tpl(newDate);
        }

        return $String(tpl ? String(tpl) : defaultTpl).format({
            d: day,
            m: month,
            y: year,
            h: hours,
            nh: normalizeHours,
            mOffset: minutesOffset,
            agoText,
            timeShort,
            min: minutes,
            s: seconds,
            daysName: daysNameValue,
            dText,
            mText: mText[parseInt(month, 10) - 1],
            mTextShort: mTextShort[parseInt(month, 10) - 1],
            mTextWhen: mTextWhen[parseInt(month, 10) - 1],
            mTextOne: mTextOne[parseInt(month, 10) - 1],
        }).toString();
    }

    public isValid(): boolean {
        return !isNaN(this.getTime());
    }
}
