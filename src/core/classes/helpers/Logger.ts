import colors from "colors";
colors.enabled = true;

interface Param {
    tpl: "{date} - [{type} {pid}] {data}";
    background: null | string;
    color: "white";
    isError: false;
    isWarn: false;
}

export default class Logger {
    private static middlewares: Array<() => any> = [];

    /**
     * Gets the current date to display
     * @returns {string}
     */
    private static getDate(): string {
        const date = new Date();
        return $Date(date).show();
    }

    private static callMiddleware(name: string, ...args): void {
        if (this.middlewares[name]) { this.middlewares[name].apply(this, args); }
    }

    /**
     * Receives a message template
     * @param textArray
     * @param param
     */
    private static getMessage(textArray, param: Param) {
        let resultText = textArray;

        if (!Helper.isString(textArray)) {
            resultText = [];
            for (const val of textArray) {
                if (val && Helper.isObject(val) && !(val instanceof Error)) {
                    try {
                        resultText.push(JSON.stringify(val, null, 4));
                    } catch (e) {
                        this.callMiddleware("error", e);
                    }
                } else if (val === null) {
                    resultText.push("null");
                } else if (Helper.isBool(val) || Helper.isObject(val) || Helper.isString(val) || Helper.isNumber(val)) {
                    resultText.push(val.toString && val.toString() || val);
                } else {
                    resultText.push("undefined");
                }
            }
        }

        let resultString = $String(param.tpl).format({
            date: Logger.getDate(),
            pid: process.pid,
            data: resultText.join(" "),
        }).toString();

        resultString = resultString[param.color || "white"];
        if (param.background) {
            resultString = resultString[param.background];
        }

        return resultString;
    }

    /**
     * Print log to console
     * @param param
     * @param arg
     */
    public static print(param: Param, ...arg) {
        const text = Logger.getMessage(Helper.isArray(arg) ? arg : $Object(arg).toArray(), param);

        if (param.isWarn) {
            return console.warn(text);
        }

        if (param.isError) {
            return console.error(text);
        }
        console.log(text);
    }

    public static addMiddleware(name, cb) {
        this.middlewares[name] = cb;
        Logger[name] = cb.bind(Logger);
    }
}
