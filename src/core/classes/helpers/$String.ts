export default class $String extends String {
    constructor(str: string | number) {
        super(str);
    }

    public getHash(): number {
        return this.split("").reduce(function (a, b) {a = ((a << 5) - a) + b.charCodeAt(0); return a & a; }, 0);
    }

    public translit(engToRus = false): $String {
        const rus = ["щ", "ш", "ч", "ц", "ю", "я", "ё", "ж", "ъ", "ы", "э", "а", "б", "в", "г", "д", "е", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ь"],
            eng = ["shh", "sh", "ch", "cz", "yu", "ya", "yo", "zh", "``", "y'", "e`", "a", "b", "v", "g", "d", "e", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "x", "`"];

        let text = String(this);
        const length = rus.length;

        for (let x = 0; x < length; x += 1) {
            text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
            text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase());
        }
        return new $String(text);
    }

    public punto(engToRus = false): $String {
        const rus = ["й", "ц", "у", "к", "е", "н", "г", "ш", "щ", "з", "х", "ъ", "ф", "ы", "в", "а", "п", "р", "о", "л", "д", "ж", "э", "я", "ч", "с", "м", "и", "т", "ь", "б", "ю", "."],
            eng = ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "z", "x", "c", "v", "b", "n", "m", ",", ".", "/"];

        let text = String(this);
        const length = rus.length;

        for (let x = 0; x < length; x += 1) {
            text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
            text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase());
        }
        return new $String(text);
    }

    public crop(size: number): $String {
        const cropped = this.slice(size);
        if (!cropped) { return this; }

        return new $String(this.slice(0, size) + cropped.replace(/^\s?([^\s]+)[\s\S]*/im, "$1..."));
    }

    public stripTags(): $String {
        return new $String(this.replace(/<\/?[^>]+>/g, ""));
    }

    public format(data: any): $String {
        const regexp = new RegExp(`\{(${Object.keys(data).join("|")})\}`, "ig");
        const str = this.replace(regexp, (match, index) =>  data[index]);

        return new $String(str);
    }

    public escape(): $String {
        return new $String(this.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"));
    }

    public escapeTags(): $String {
        return new $String(this
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;"));
    }

    public replaceNewLines(): $String {
        return new $String(this
            .replace(/[\n↵]/g, "\\n")
            .replace(/\r/g, "\\r")
            .replace(/\t/g, "\\t")
            .replace(/ {4,}/g, " "));
    }

    public toNumber(): number {
        const res = parseFloat(this.trim().replace(/\s/ig, "").replace(",", "."));
        return isNaN(res) ? null : res;
    }

    public toUrl(url, https = false): string {
        let res = this.trim();
        if (!res.match(/^http/i)) { res = `http${https ? "s" : ""}://${res}`; }

        return res + "/" + (url || "");
    }

    public ofJson(): any {
        try {
            return JSON.parse(this.toString());
        } catch (e) {
            return null;
        }
    }
}
