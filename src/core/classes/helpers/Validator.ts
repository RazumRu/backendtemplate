export default class Validator {
    private messages = [];
    private validateStatus = true;
    private static customTypes = new Map<string, {message: string, validate: (value: any) => boolean}>();

    public static addCustomType(name: string, message: string, validate: (value: any) => boolean) {
        this.customTypes.set(name, {message, validate});
    }

    public static getCustomType(name: string) {
        return this.customTypes.get(name);
    }

    constructor(private data: any) {}

    public addMessage(message) {
        this.validateStatus = false;
        this.messages.push(message);
    }

    public isValid() {
        return this.validateStatus;
    }

    public getMessage(successText?: string) {
        if (!successText) {
            successText = Lang.get("validate.success", null, "Verification successful");
        }
        return this.isValid() ? successText : this.messages.join("; ");
    }

    /**
     * Checks variables by existence in the list(OR)
     * @param existsGroup
     */
    public existsOneOf(existsGroup: Array<{name: string, fields: Array<string>}>) {
        for (const group of existsGroup) {
            let status = true;
            for (const field of group.fields) {
                if (empty(this.data[field])) {
                    status = false;
                    break;
                }
            }

            if (status) {
                return this;
            }
        }

        this.addMessage(Lang.get("validate.emptyFieldData", {list: $Object(existsGroup.map((el) => el.name)).toArray().join(", ")}, "One of the fields is not passed: {list}"));

        return this;
    }

    /**
     * Valid types var
     * @param type
     * @param value
     * @param name
     */
    public customType(type: string, value: any, name: string) {
        const validate = Validator.getCustomType(type);

        if (validate) {
            if (!validate.validate(value)) {
                this.addMessage($String(validate.message).format({
                    name,
                }));
            }
        }

        return this;
    }

    /**
     * Valid regular
     * @param regulars key: [regular as text, var name]
     * @returns {boolean}
     */
    public regular(regulars) {
        const res = [];

        for (const name in regulars) {
            if (!regulars.hasOwnProperty(name)) {  continue; }

            const reg = regulars[name];
            if (!this.data[name] || !(new RegExp(reg[0], "ig").test(this.data[name]))) {
                res.push(reg[1]);
            }
        }

        if (res.length) {
            this.addMessage(Lang.get("validate.types.regularMany", {list: res.join(", ")}, "Values contain invalid characters: {list}"));
        }

        return this;
    }

    /**
     * Compares 2 values by name
     * @param a
     * @param b
     * @param nameA
     * @param nameB
     * @returns {Validator}
     */
    public equal(a, b, nameA, nameB) {
        const isEqual = this.data[a].toString() === this.data[b].toString();

        if (!isEqual) {
            this.addMessage(Lang.get("validate.types.notEqual", {a: (a || nameA), b: (b || nameB)}, "Values {a} and {b} are not equal"));
        }

        return this;
    }

    /**
     * Custom validate
     * @param message
     * @param callback
     */
    public custom(message, callback) {
        const res = callback(this.data);
        if (!res) {
            this.addMessage(message);
        }

        return this;
    }

    /**
     * example: {
     *     key: Number,
     *     emailKey: "email",
     *     "customKey?": Array,
     *     messageKey: {type: Date, name: "message key"}
     * }
     * @param variables
     */
    public validate(variables: any) {
        const existFields = [];

        for (let key in variables) {
            let type = variables[key];
            let name = key;
            const isExist = !key.endsWith("?");
            key = key.replace(/\?$/g, "");
            let value = this.data[key];

            if (Helper.isObject(type) && type.type) {
                if (type.name) {
                    name = type.name;
                }

                type = type.type;
            }

            // empty?
            if (isExist && Helper.empty(value)) {
                existFields.push(name);
                continue;
            } else if (Helper.empty(value)) {
                continue;
            }

            if (Helper.isObject(type) || Helper.isFunc(type)) {
                switch (type.name) {
                    case "Number":
                        if (!/^[\d\.]+$/.test(value)) {
                            this.addMessage(Lang.get("validate.types.number", {name}, "The {name} field must be a number"));
                        } else {
                            this.data[key] = parseFloat(value);
                        }
                        break;

                    case "Date":
                        if (!$Date(value).isValid()) {
                            this.addMessage(Lang.get("validate.types.date", {name}, "The ${name} field must be the date"));
                        } else {
                            this.data[key] = new Date(value);
                        }
                        break;

                    case "String":
                        if (!Helper.isString(value)) {
                            this.addMessage(Lang.get("validate.types.string", {name}, "The {name} field must be a string"));
                        } else {
                            this.data[key] = String(value).trim();
                        }
                        break;

                    case "Object":
                        if (!Helper.isObject(value)) {
                            this.addMessage(Lang.get("validate.types.object", {name}, "The {name} field must be a object"));
                        }
                        break;

                    case "Boolean":
                        if (Helper.isNumber(value)) {
                            value = !!value;
                        }
                        if (!Helper.isBool(value)) {
                            this.addMessage(Lang.get("validate.types.boolean", {name}, "The {name} field must be a boolean"));
                        } else {
                            this.data[key] = !!value;
                        }
                        break;

                    case "Array":
                        if (!Helper.isArray(value)) {
                            this.addMessage(Lang.get("validate.types.array", {name}, "The {name} field must be a array"));
                        }
                        break;

                    default:
                        this.addMessage("Undefined validate type: " + (type.name || type));
                }
            } else {
                this.customType(type, value, name);
            }
        }

        if (existFields.length) {
            this.addMessage(Lang.get("validate.emptyData", {list: existFields.join(", ")}, "No data transferred: {list}"));
        }

        return this.isValid();
    }
}
