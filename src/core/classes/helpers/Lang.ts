export default class Lang {
    public static get(index: string, data?: any, defaultValue?: string) {
        let str = $Object(Config.lang).getProp(index) || defaultValue;
        if (!empty(str) && data) { str = $String(str).format(data || {}); }
        return str;
    }
}
