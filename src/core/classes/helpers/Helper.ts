export default class Helper {
    /**
     * get env variables
     * @param paramName
     * @param defaultValue
     */
    public static env(paramName: string, defaultValue: string|number): string|number|null {
        return this.default(process.env[paramName], defaultValue);
    }

    /**
     * Inclines word depending on the number
     * @param count
     * @param titles
     * @returns {*}
     */
    public static declOfNum(count: number, titles: Array<string>): string {
        count = Math.abs(count);
        const cases = [2, 0, 1, 1, 1, 2];
        return titles[ (count % 100 > 4 && count % 100 < 20) ? 2 : cases[(count % 10 < 5) ? count % 10 : 5] ];
    }

    /**
     * Checks for emptiness
     * @param value
     * @name empty
     * @returns {boolean}
     */
    public static empty(value): boolean {
        return typeof value === "undefined" || value === null || (this.isString(value) && value === "") || (this.isNumber(value) && isNaN(value));
    }

    public static isObject(value): boolean {
        return value && typeof value === "object";
    }

    public static isArray(value): boolean {
        return value && value instanceof Array;
    }

    public static isFunc(value): boolean {
        return typeof value === "function";
    }

    public static isBool(value): boolean {
        return typeof value === "boolean";
    }

    public static isString(value): boolean {
        return typeof value === "string";
    }

    public static isNumber(value): boolean {
        return typeof value === "number";
    }

    public static isDate(value): boolean {
        return value && value instanceof Date;
    }

    public static isBuffer(value): boolean {
        return value && value instanceof Buffer;
    }

    public static base64_encode(text: string): string {
        return (Buffer.from((text || "").toString(), "utf8")).toString("base64");
    }

    public static base64_decode(text: string): string {
        return (Buffer.from(text.trim(), "base64")).toString("utf8");
    }

    /**
     * Sets the variable to the default value.
     * @param value
     * @param def
     */
    public static default(value, def): any {
        return !this.empty(value) ? value : def;
    }

    public static random (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public static getRandomString(length = 10) {
        const str = "abcdefghijklmnopqrstuvwxyz123456789";
        let result = "";

        for (let i = 0; i < length; i++) {
            result += str[Helper.random(0, str.length - 1)];
        }

        return result;
    }
}
