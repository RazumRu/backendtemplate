export default class $Number extends Number {
    constructor(numb: string | number) {
        super(numb);
    }

    public show(valute?: string, fixed = 2, nbsp = true): string {
        if (valute === null || valute === undefined) { valute = "руб."; }
        const price = this.toFixed(fixed);
        const res = price.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1" + (nbsp ? "&nbsp;" : " "));

        return res + (nbsp ? "&nbsp;" : " ") + valute;
    }

    public secondsToString(showSec = false): string {
        const minutes = Math.floor(Number(this) / 60);
        const seconds = Math.floor(Number(this) % 60);
        const hour = Math.floor(minutes / 60);
        let min: any = minutes % 60;
        if (min < 10) { min = "0" + min; }

        let text = `${hour} ч ${min} м`;
        if (showSec) { text += ` ${seconds} с`; }
        return text;
    }
}
