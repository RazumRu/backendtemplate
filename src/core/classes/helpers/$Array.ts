import Helper from "@core/classes/helpers/Helper";

export default class $Array {
    constructor(private array: Array<any>) {
    }

    public merge(...arrays: Array<any>): Array<any> {
        return _.union(this.array, ...arrays);
    }

    public mergeStrings(...arrays: Array<any>): Array<any> {
        const target = [];
        for (const el of this.array) {
            target.push(Helper.isObject(el) ? $String($Object(el).toJson()).getHash() : String(el));
        }

        const mergeArrays = [];
        for (const arr of arrays) {
            mergeArrays.push(arr.map((el) => Helper.isObject(el) ? $String($Object(el).toJson()).getHash() : String(el)));
        }

        return _.union(target, ...mergeArrays);
    }

    public search(values: Array<string | number> | string | number): boolean {
        if (!Helper.isArray(values)) {
            // @ts-ignore
            values = [values];
        }

        let indOf;

        // @ts-ignore
        for (const val of values) {
            indOf = this.array.indexOf(val);
            if (indOf === -1) {
                return false;
            }
        }

        return true;
    }

    public searchByString(values: Array<string | number> | string | number): boolean {
        if (!Helper.isArray(values)) {
            // @ts-ignore
            values = [values];
        }

        let indOf;
        const arr = this.array.map((val) => String(val));

        // @ts-ignore
        for (const val of values) {
            indOf = arr.indexOf(String(val));
            if (indOf === -1) {
                return false;
            }
        }

        return true;
    }

    public removeVal(value: Array<any> | string | number): Array<any> {
        if (!Helper.isArray(value)) { value = [value]; }
        return _.pullAll(this.array, value as Array<any>);
    }

    public toObject(callback: (val: any) => Array<any>): any {
        const result = {};

        let i = 0;
        for (const elem of this.array) {
            const data = callback(elem);
            let index, value;

            if (Helper.isArray(data)) {
                index = data[0];
                value = data[1];
            } else {
                index = i++;
                value = data;
            }

            result[index] = value;
        }

        return result;
    }

    public getRandom(): any {
        return this.array[Math.floor(Math.random() * this.array.length)];
    }

    public removeDuplicateValues(convertToString = false): Array<any> {
        let target = this.array;

        if (convertToString) {
            target = [];
            for (const el of this.array) {
                target.push(Helper.isObject(el) ? $String($Object(el).toJson()).getHash() : String(el));
            }
        }

        return _.uniq(target);
    }
}
