export default class $Fn {
    constructor(private fn?: any) {
    }

   public cbPromise(context: any, ...arg): Promise<any> {
       return new Promise((resolve, reject) => {
           arg.push((err, data) => {
               if (err) { return reject(err); }
               resolve(data);
           });
           this.fn.apply(context, arg);
       });
   }
}
