import InitService from "@core/classes/services/InitService";

/**
 * path generate helper
 */
export default class Path {
    public static getPath(path: string): string {
        path = InitService.replacePathAlias(path);

        return path;
    }
}
