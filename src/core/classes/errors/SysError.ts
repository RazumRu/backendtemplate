import ParentError from "@core/classes/errors/ParentError";

export default class SysError extends ParentError {
    constructor(message: string, err?: any) {
        super(500, message || Lang.get("errors.sysErrorDefaultMessage", null, "System Error"), err);
        this.name = "SysError";
    }
}
