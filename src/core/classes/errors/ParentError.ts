import Helper from "@core/classes/helpers/Helper";

export default class ParentError extends Error {
    private stackLineCount = 5;
    public number: number;
    public code: number;
    public parentError: Error;
    public fileName: string;
    public lineNumber: number;
    public errors: Array<any>;

    constructor(code: number, message?: string, err?: any) {
        super();
        this.number = code;

        if (err) {
            this.parentError = err;
            if (err.number) { this.number = err.number; }
            if (err.fileName) { this.fileName = err.fileName; }
            if (err.lineNumber) { this.lineNumber = err.lineNumber; }
        }

        this.message = message;

        if (err) {
            this.message += ": " + err.toString();
        }
    }

    public toString(): string {
        const messages = [];
        const stackFragments = this.parseStack(this.stack);
        const parentStackFragments = this.parseStack(this.parentError && this.parentError.stack);

        for (const err of (this.errors || [this.message])) {
            if (Helper.isObject(err)) {
                messages.push(err.reason ? err.reason.message : err.message);
            } else {
                messages.push(err);
            }
        }

        let message = `${this.name}: ${messages.join("; ")}`;

        if (stackFragments.length) {
            message += `\n\tstack:\n${stackFragments.splice(0, this.stackLineCount).map((f) => this.formatStackLine(f)).join("\n")}`;
        }

        if (parentStackFragments.length) {
            message += `\n\tparentStack:\n${parentStackFragments.splice(0, this.stackLineCount).map((f) => this.formatStackLine(f)).join("\n")}`;
        }

        return message;
    }

    protected formatStackLine(line: {fn: string, file: string, line: number, pos: number}): string {
        return `\tat ${line.fn} (${line.file}:${line.line}:${line.pos})`;
    }

    protected parseStack(stack: string): Array<{fn: string, file: string, line: number, pos: number}> {
        if (!stack) { return []; }

        const result = [];
        const fragments = stack.split("\n");

        for (const fragment of fragments) {
            const match = fragment.trim().match(/^at ([^\s]+) \((\S+):(\d+):(\d+)\)$/i);

            if (!match) { continue; }

            result.push({
                fn: match[1],
                file: match[2],
                line: parseInt(match[3], 10),
                pos: parseInt(match[4], 10),
            });
        }

        return result;
    }
}
