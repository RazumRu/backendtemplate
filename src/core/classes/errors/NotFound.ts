import ParentError from "@core/classes/errors/ParentError";

export default class NotFound extends ParentError {
    constructor(message: string, err?: any) {
        super(404, message || Lang.get("errors.notFoundDefaultMessage", null, "NotFound"), err);
        this.name = "NotFound";
    }
}
