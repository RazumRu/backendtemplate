import ParentError from "@core/classes/errors/ParentError";

export default class HttpError extends ParentError {
    constructor(code: number, message?: string, err?: any) {
        super(code, message || Lang.get("errors.httpErrorDefaultMessage", null, "Http Error"), err);
        this.name = "HttpError";
    }
}
