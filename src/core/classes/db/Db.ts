import MongooseModel from "@core/classes/db/MongooseModel";
import async from "async";
import fs from "fs";
import Knex from "knex";
import mongoose from "mongoose";
import requireAll from "require-all";

export default class Db {
    private static mongooseModels: any = {};
    public static connectionKnex: Knex;
    public static connectionMongoose: mongoose;

    public static async connectKnex() {
        const connectInfo = Config.connections.db.knex;
        this.connectionKnex = Knex(connectInfo);

        Logger.info("connect to knex", connectInfo);
        return this.connectionKnex;
    }

    public static async connectMongo() {
        const connectInfo = Config.connections.db.mongoose;
        this.connectionMongoose = mongoose;
        const connectionString = `mongodb://${connectInfo.host}:${connectInfo.port}/${connectInfo.database}`;
        const options = {
            user: connectInfo.user,
            pass: connectInfo.password,
            keepAlive: true,
            autoIndex: false,
            useNewUrlParser: true,
        };

        if (DEBUG) {
            this.connectionMongoose.set("debug", function (coll, method, query, doc) {
                Logger.db(coll, method, query && $Object(query).toJson(), doc && $Object(doc).toJson());
            });
        }

        this.connectionMongoose.set("useCreateIndex", false);
        this.connectionMongoose.set("useFindAndModify", false);

        // mongoose.Promise = global.Promise;
        await this.connectionMongoose.connect(connectionString, options);

        Logger.sysInfo("Connect to mongoose", connectInfo);

        return this.connectionMongoose;
    }

    public static insureIndex() {
        return new Promise((res, rej) => {
            async.each(Object.keys(this.mongooseModels), (modelName, callback) => {
                this.mongooseModels[modelName].createIndexes(callback);
            }, (err) => {
                if (err) { return rej(err); }
                res();
            });
        });
    }

    public static addTypeMongoose(type: (val: any) => void) {
        const name = type.name;

        const func = (new Function("name", "return function " + name + "(key, options) { Db.mongoose.SchemaType.call(this, key, options, name);}"))(name);
        func.prototype = Object.create(this.getMongo().SchemaType.prototype);
        func.prototype.cast = type;

        this.getMongo().Schema.Types[name] = func;
    }

    public static getKnex(): Knex {
        return this.connectionKnex;
    }

    public static getMongo(): mongoose {
        return this.connectionMongoose;
    }

    public static addModelsMongo(modelsPath: string): void {
        if (fs.existsSync(modelsPath)) {
            const models: any = requireAll({
                dirname: modelsPath,
                recursive: true,
            });

            for (const modelName in models) {
                if (!models.hasOwnProperty(modelName)) { continue; }

                const model = models[modelName];
                const instance: MongooseModel = new (model.default || model)();

                if (instance.createModel) {
                    this.mongooseModels[modelName] = instance.createModel();
                }
            }
        }
    }

    public static getMongoModel(name: string): MongooseModel {
        return this.mongooseModels[name];
    }

    public static getMongoModels(): any {
        return this.mongooseModels;
    }
}
