import Db from "@core/classes/db/Db";
import MongooseActions from "@src/db/mongoose/plugins/MongooseActions";
import MongooseHelpers from "@src/db/mongoose/plugins/MongooseHelpers";
import MongooseHidden from "@src/db/mongoose/plugins/MongooseHidden";
import MongooseQueue from "@src/db/mongoose/plugins/MongooseQueue";
import MongooseRedisCache from "@src/db/mongoose/plugins/MongooseRedisCache";
// @ts-ignore
import beautifyUnique from "mongoose-beautiful-unique-validation";

export default class MongooseModel {
    [key: string]: any;

    protected fields = {
        createDate: {
            type: Date,
            default: Date.now,
        },
        modified: {
            type: Date,
            default: Date.now,
        },
    };

    private virtualFields = {};
    private methods = [];
    private indexes = [];

    private doSaveCallbacks: Array<() => Promise<void>> = [];
    private doRemoveCallbacks: Array<() => Promise<void>> = [];
    private clearCacheCallbacks: Array<() => Promise<void>> = [];

    private tableName: string;
    private schemaName: string;
    private notFoundMessage: string;
    private cacheOptions: {seconds: number};

    /**
     * Создает схему таблицы
     * @returns {*}
     */
    private createSchema() {
        const mongoose = Db.getMongo();
        const schema = new mongoose.Schema(this.fields || {}, {
            minimize: false,
            usePushEach: true,
            id: true,
        });
        schema.set("toObject", { virtuals: true });
        schema.set("toJSON", { virtuals: true });

        for (const index in this.virtualFields) {
            if (!this.virtualFields.hasOwnProperty(index)) { continue; }
            schema.virtual(index, this.virtualFields[index]);
        }

        for (const index of this.indexes) {
            schema.index(index);
        }

        for (const cb of this.doSaveCallbacks) {
            schema.pre("save", cb);
        }
        this.doSaveCallbacks = [];

        for (const cb of this.doRemoveCallbacks) {
            schema.pre("save", cb);
        }
        this.doRemoveCallbacks = [];

        schema.virtual("tableName").get(() => {
            return this.getTableName();
        });

        if (this.cacheOptions) {
            schema.set("redisCache", true);
            schema.set("expires", this.cacheOptions.seconds);
        }

        if (this.schemaName) { schema.set("schemaName", this.schemaName); }
        if (this.notFoundMessage) { schema.set("notFoundMessage", this.notFoundMessage); }
        schema.set("clearCacheCallbacks", this.clearCacheCallbacks);

        const methods = this.getMethods();
        for (const method of methods) {
            schema.methods[method.name] = schema.statics[method.name] = method;
        }

        schema.plugin(beautifyUnique);
        schema.plugin(MongooseHidden.run);
        schema.plugin(MongooseHelpers.run);
        schema.plugin(MongooseQueue.run);
        schema.plugin(MongooseRedisCache.run);
        schema.plugin(MongooseActions.run);

        return schema;
    }

    /**
     * Возвращает методы, они будут гибридными(одновременно статическими и объектными)
     * @returns {Object}
     */
    private getMethods() {
        return this.methods;
    }

    /**
     * Добавлячет метод
     */
    private addMethods(...methods) {
        for (const method of methods) {
            this.methods.push(method);
        }
    }

    /**
     * Создает готовую модель
     * @returns {*|Aggregate|Model}
     */
    public createModel() {
        const schema = this.createSchema();
        Logger.db("Create mongoose model", this.getTableName());
        return Db.getMongo().model(this.getTableName(), schema);
    }

    /**
     * Возвращает имя таблицы. Ищет в текущем контексте свойство table.
     * Если его нет, берет название конструктора
     * @returns {*}
     */
    protected getTableName(): string {
        return this.tableName || this.constructor.name;
    }

    /**
     * Добавлячет поле
     * @param name
     * @param data
     */
    protected addField(name: string, data: {type: any, unique?: string, required?: string, enum?: Array<string | number>, hideJSON?: boolean, default?: any}) {
        const fieldProp: any = data;

        if (data.required) {
            fieldProp.required = [true, data.required];
        }

        this.fields[name] = fieldProp;
    }

    protected addIndex(index: any) {
        this.indexes.push(index);
    }

    /**
     * Автоматически ищет все методы класса и добавляет их к модели
     */
    protected searchMethods(object?: any) {
        if (!object) {
            // @ts-ignore
            object = this.__proto__;
        }

        const methods = Object.getOwnPropertyNames(object).filter((prop) => typeof object[prop] === "function" && prop !== "constructor");

        this.addMethods.apply(this, methods.map((name) => object[name]));
    }

    /**
     * Добавлячет виртуальное поле
     * @param name
     * @param data
     */
    protected addVirtualField(name: string, data: { ref: string, localField: string, foreignField: string, justOne?: boolean}) {
        this.virtualFields[name] = data;
    }

    /**
     * Генерирует на лету валидатор для проверки регулярного выражения
     * @param regExp
     * @param message
     * @returns {*}
     */
    protected getRegExpValidator(regExp: RegExp, message: string) {
        if (!regExp) { return null; }
        if (!message) { message = "Значение {VALUE} заполнено неверно"; }

        return {
            validator(v) {
                return regExp.test(v);
            },
            message,
        };
    }

    /**
     * Включает кешиование
     * @param seconds
     */
    protected cache(seconds: number) {
        Logger.info("cache Redis on for " + this.constructor.name + " (" + seconds + "s)");

        this.cacheOptions = {
            seconds,
        };
    }

    /**
     * Очистка кеша
     * @param cb
     */
    protected clearCacheCallback(cb: () => Promise<void>) {
        this.clearCacheCallbacks.push(cb);
    }

    /**
     * Задает имя схемы, то есть название сущности для отображения в различных сообщениях
     * @param name
     */
    protected setSchemaName(name: string) {
        this.schemaName = name;
    }

    /**
     * Задает сообщение, если элемент не найден
     * @param name
     */
    protected setNotFoundMessage(name: string) {
        this.notFoundMessage = name;
    }

    /**
     * Добвыляет каллбек перед сохранением
     * @param doSaveCallback
     */
    protected doSave(doSaveCallback: () => Promise<void>) {
        this.doSaveCallbacks.push(doSaveCallback);
    }

    /**
     * Добвыляет каллбек перед удалением
     * @param doRemoveCallback
     */
    protected doRemove(doRemoveCallback: () => Promise<void>) {
        this.doRemoveCallbacks.push(doRemoveCallback);
    }

    protected getType(index: string): any {
        if (!Db.connectionMongoose.Schema.Types[index]) {
            throw new NotFound(`Type ${index} not found`);
        }

        return Db.connectionMongoose.Schema.Types[index];
    }
}
