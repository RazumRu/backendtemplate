import async from "async";

/**
 * Asynchronous queues of the same type of requests
 */
export default class RequestsQueue {
    private static queue = {};
    private static process = {};

    /**
     * Adds an item to the queue
     * @param key
     * @param callback
     */
    public static addQueue(key, callback) {
        if (!this.queue[key]) { this.queue[key] = []; }
        this.queue[key].push(callback);
    }

    /**
     * Receives the number of messages in the queue
     * @returns {number}
     */
    public static getCountQueue(key) {
        return this.queue[key] ? this.queue[key].length : 0;
    }

    /**
     * Checks the queue for existence
     * @param key
     */
    public static emptyQueue(key) {
        return empty(this.queue[key]);
    }

    /**
     * Causes a queue
     * @param key
     * @param context
     * @param arg
     */
    public static async callQueue(key, context, ...arg) {
        const list = this.queue[key] || [];
        if (this.process[key]) { return; }
        this.process[key] = true;

        const queueGroupSize = Config.cache.queueGroupSize;
        // Logger.debug('callQueue', list.length);

        let cb = list.shift();
        let pack = [];
        while (cb) {
           // Logger.debug('callElemQueue', list.length);
            pack.push(cb);

            if (pack.length === queueGroupSize || !list.length) {
                await new Promise((resolve, reject) => {
                    setImmediate(() => {
                        async.eachLimit(pack, queueGroupSize, (elemCb, callback) => {
                            elemCb.apply(context, arg).then(callback).catch((err) => {
                                // Logger.error(err);
                                callback();
                            });
                        }, (err) => {
                            if (err) { Logger.error(err); }
                            // Logger.debug('endPackQueue', list.length);
                            pack = [];
                            resolve();
                        });
                    });
                });
            }

            cb = list.shift();
        }
        delete this.queue[key];
        delete this.process[key];
    }
}

module.exports = RequestsQueue;
