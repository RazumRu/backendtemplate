export default class AuthService {
    // Стратегии авторизации
    private static strategies = [];

    constructor(private data: any, private socket: any) {}

    /**
     * Adds an automation strategy
     * @param name
     * @param middleware
     */
    public static addStrategy(name, middleware) {
       this.strategies[name] = middleware;
    }

    public static getStrategy(name) {
        return this.strategies[name] || null;
    }

    /**
     * Checks user by strategy
     * @param name
     */
    public check(name) {
        const strategy = AuthService.getStrategy(name);

        if (!strategy) { throw new SysError(Lang.get("auth.strategyNotFound", null, "Authorization strategy not found")); }
        return strategy(this.data, this.socket);
    }
}
