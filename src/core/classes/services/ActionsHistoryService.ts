import {ContextData} from "../interfaces/ServerInterfaces";

/**
 * История децствий пользователей
 */
export default class ActionsHistoryService {
    private static middleware = {};
    private static modelName = "Action";

    /**
     * Добавляет обработчик для события
     * @param type
     * @param callback
     */
    public static addAction(type, callback: (contextData: ContextData, data: any) => Promise<any>) {
        this.middleware[type] = callback;
    }

    /**
     * Запускает событие
     * @param type
     * @param contextData
     * @param data
     * @returns {*}
     */
    public static async action(type, contextData: ContextData, data) {
        if (!this.middleware[type]) {
            throw new NotFound("Обработчик " + type + " не найден!");
        }

        try {
            const dataLog = await this.middleware[type](contextData, data);

            await this.log(type, contextData, dataLog);
        } catch (err) {
            Logger.error(new SysError("Ошибка добавления события", err));
            throw err;
        }
    }

    /**
     * Логирует событие
     * @param type
     * @param req
     * @param dataAction
     */
    public static async log(type, req, dataAction) {
        const logAction = new (Db.getMongoModel(this.modelName))({
            type,
            user: req && req.userService.getSchemaType(),
            message: dataAction.message,
            source: dataAction.source,
            sourceId: dataAction.sourceId,
            data: dataAction.data,
        });

        return logAction.save();
    }
}

module.exports = ActionsHistoryService;
