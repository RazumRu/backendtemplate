import fs from "fs";
import InitService from "./InitService";

export default class ConsoleCommandsService {
    public static runCommand(name: string, args) {
        const paths = [
            `@core/scripts/console/${name}`,
        ];

        let _class;
        for (const path of paths) {
            const pathAlias = InitService.replacePathAlias(path);
            if (!fs.existsSync(pathAlias + ".js") && !fs.existsSync(pathAlias)) { continue; }
            _class = require(pathAlias);
            break;
        }

        if (!_class) { throw new NotFound("console class not found"); }
        _class = _class.default || _class;
        const instance = new _class(args);
        if (!instance.run) { throw new NotFound(`Missing run method`); }

        return instance.run();
    }
}