import nodemailer from "nodemailer";

/**
 * send emails
 */
export default class EmailService {
    private transport;
    private readonly from: string;

    constructor(param: {from: string, host?: string, port?: number, service?: string, login: string, pass: string}) {
        if (Config.services && Config.services.email) {
            $Object(param).setDefaultProps({
                from: Config.services.email.login,
                service: Config.services.email.service,
                login: Config.services.email.login,
                pass: Config.services.email.pass,
            });
        }

        this.transport = nodemailer.createTransport({
            service: param.service,
            host: param.host,
            port: param.port,
            auth: {
                user: param.login,
                pass: param.pass,
            },
        });
        this.from = param.from;
    }

    public async send(data: {to: Array<string>, cc?: Array<string>, bcc?: Array<string>, title?: string, body: string, attachments: Array<any>, replyTo?: string, inReplyTo?: string, references?: Array<string>}) {
        const mailOptions = {
            from: this.from,
            to: data.to.join(","),
            cc: data.cc && data.cc.join(","),
            bcc: data.bcc && data.bcc.join(","),
            subject: data.title,
            html: data.body,
            attachments: data.attachments || [],
            replyTo: data.replyTo,
            inReplyTo: data.inReplyTo,
            references: data.references,
        };

        Logger.debug("Message sent", data.to, data.title);
        return await this.transport.sendMail(mailOptions);
    }
}
