import jwt from "jsonwebtoken";
import uuid from "uuid/v4";

/**
 * JWT authorization implementation
 */
export default class JWTService {
    public static generateToken(data, expiresIn?: number) {
        const param: any = {};
        if (expiresIn) {
            param.expiresIn = expiresIn;
        }
        return jwt.sign(data, Config.secretKey, param);
    }

    public static generateRefreshToken() {
        return uuid();
    }

    public static verifyToken(token: string) {
        try {
            return jwt.verify(token, Config.secretKey);
        } catch (e) {
            return false;
        }
    }

    public static decodeToken(token: string) {
       return jwt.decode(token);
    }
}
