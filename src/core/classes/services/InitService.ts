// argv
import dotenv from "dotenv";
import fs from "fs";
import moduleAlias from "module-alias";
import optimist from "optimist";
import path from "path";
import requireAll from "require-all";
import Config from "../../config";
import Helper from "../helpers/Helper";
const SEP = path.sep;

/**
 * Instance initialization
 */
export default class InitService {
    private static pathsAliases = {};

    /**
     * Gets the path alias
     * @param alias
     */
    public static getPathAlias(alias) {
        return this.pathsAliases[alias];
    }

    /**
     * Replaces path aliases in string.
     * @param pathValue
     */
    public static replacePathAlias(pathValue) {
        for (const index in this.pathsAliases) {
            if (!this.pathsAliases.hasOwnProperty(index)) { continue; }
            const alias = this.pathsAliases[index];
            pathValue = pathValue.replace(index, alias);
        }

        return path.resolve(pathValue);
    }

    /**
     * Applies the current instance, sets aliases.
     */
    public static initApp(appName = null) {
        const args = this.getArgsApp();

        if (!appName) {
            if (!args._[0]) { throw new Error("Instance not passed"); }
            appName = args._[0];
        }
        this.addGlobal("APP_NAME", appName);

        const pathSrc = __dirname + "/../../..";

        this.pathsAliases = {
            "@src": pathSrc,
            "@root": pathSrc + "/..",
            // only works in dist
            "@projectRoot": pathSrc + "/../..",
            "@core": pathSrc + "/core",
            "@app": pathSrc + "/apps/" + APP_NAME,
            "@apps": pathSrc + "/apps",
        };
        moduleAlias.addAliases(this.pathsAliases);
        moduleAlias.addPath(pathSrc);

        let envPath = this.replacePathAlias("@projectRoot/.env");
        if (!fs.existsSync(envPath)) {  envPath = this.replacePathAlias("@root/.env"); }
        dotenv.config({
            path: envPath,
        });

        const env = process.env.NODE_ENV;
        const isDevFromEnv = env && env === "development";

        this.addGlobal("DEV", Helper.default(isDevFromEnv, args.dev));
        InitService.addGlobal("env", Helper.env.bind(Helper));
    }

    /**
     * Creates a configuration variable
     */
    public static createConfig() {
        const index = DEV ? "dev" : "prod";
        const paths = [
            `@core/config/index.js`,
            `@core/config/${index}/index.js`,
            `@app/config/index.js`,
            `@app/config/${index}/index.js`,
        ];

        let config = {};
        for (const path of paths) {
            const pathAlias = this.replacePathAlias(path);
            if (!fs.existsSync(pathAlias)) { continue; }
            const cfg = require(path);
            if (cfg && typeof cfg === "object") { config = this.concatOptions(cfg.default || cfg, config); }
        }

        InitService.addGlobal("Config", config);

        return config;
    }

    /**
     * Gets the arguments of the current instance
     */
    public static getArgsApp() {
       return optimist.argv;
    }

    /**
     * Adds a variable to the global scope.
     * @param name
     * @param value
     */
    public static addGlobal(name: string, value: any) {
        Object.defineProperty(global, name, {
            get: () => value,
        });
    }

    /**
     * Enables initialization files
     * @param files
     */
    public static async startInit(files?: Array<string>) {
        const paths = [
            `@core/init`,
            `@app/init`,
        ];

        const connectedFiles = [];

        if (Helper.isArray(files)) {
            for (const pathFile of files) {
                for (const path of paths) {
                    const pathAlias = this.replacePathAlias(`${path}/${pathFile}`);
                    if (!fs.existsSync(pathAlias)) { continue; }
                    let fn = require(pathAlias);
                    if (fn && fn.default) { fn = fn.default; }
                    if (fn && Helper.isFunc(fn)) { await fn(); }
                    connectedFiles.push(pathAlias.split(SEP).pop());
                }
            }
        }

        for (const path of paths) {
            const pathAlias = this.replacePathAlias(path);
            if (!fs.existsSync(pathAlias)) { continue; }
            requireAll({
                dirname: pathAlias,
                filter : (fileName) => $Array(connectedFiles).search(fileName),
            });
        }
    }

    /**
     * Connects interlayer
     * [['core', 'nameMiddleware'],['app', 'nameMiddleware']]
     * @param files
     */
    public static initMiddlewares(files?: Array<Array<string>>) {
        const paths = [
            `@core/init/middlewares`,
            `@app/init/middlewares`,
        ];

        if (Helper.isArray(files)) {
            for (const pathInfo of files) {
                const currentPath = `@${pathInfo[0]}/init/middlewares/${pathInfo[1]}`;
                require(currentPath);
            }
        } else {
            for (const path of paths) {
                const pathAlias = this.replacePathAlias(path);
                if (!fs.existsSync(pathAlias)) { continue; }
                requireAll(pathAlias);
            }
        }
    }

    /**
     * Connect options
     * @param target
     * @param options
     * @returns {*}
     */
    private static concatOptions(target, ...options) {
        if (options) {
            for (const i in options) {
                if (!options.hasOwnProperty(i)) { continue; }
                const opt = options[i];
                if (!opt) { continue; }

                for (const name in opt) {
                    if (!opt.hasOwnProperty(name)) { continue; }

                    const val = opt[name];
                    if (Helper.empty(target[name])) {
                        target[name] = val;
                        continue;
                    }

                    if (Helper.isArray(val)) {
                        function getVal(val) {
                            return Helper.isObject(val) ? JSON.stringify(val) : String(val);
                        }
                        const _target = target[name].map((val) => getVal(val));

                        for (const i in val) {
                            if (!val.hasOwnProperty(i)) { continue; }
                            const v = val[i];
                            const indOf = _target.indexOf(getVal(v));
                            if (indOf === -1) { target[name].push(v); }
                        }

                        continue;
                    }

                    if (Helper.isObject(val)) {
                        target[name] = this.concatOptions(target[name], val);
                    }
                }
            }
        }
        return target;
    }
}
