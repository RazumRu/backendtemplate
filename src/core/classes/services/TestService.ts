import fs from "fs";
import Mocha from "mocha";
import InitService from "./InitService";

export default class TestService {
    public static runTest(name: string) {
        const path = `@core/scripts/test/${name}`;

        const pathAlias = InitService.replacePathAlias(path);
        if (!fs.existsSync(pathAlias + ".js") && !fs.existsSync(pathAlias)) {
            throw new NotFound("Test file not found");
        }

        const mocha = new Mocha();
        mocha.addFile(pathAlias);
        return mocha.run();
    }
}
