import admin from "firebase-admin";
import request from "request-promise";

export default class FirebaseService {
    private initStatus = false;

    constructor(private keyPath: string, private config: {databaseURL: string, serverKey: string}) {}

    public init() {
        if (this.initStatus) {
            return;
        }

        const serviceAccount = require(this.keyPath);

        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: this.config.databaseURL,
        });

        this.initStatus = true;
    }

    public async verifyToken(token: string) {
        try {
            const body = await request({
                url: "https://iid.googleapis.com/iid/info/" + token,
                json: true,
                headers: {
                    Authorization: `key=${this.config.serverKey}`,
                },
            });

            return true;
        } catch (e) {
            return false;
        }
    }

    public async sendMessage(token: string, notification?: {title?: string, body?: string}, data?: Object) {
        const message: any = {
            data,
            notification,
            token,
        };

        return admin.messaging().send(message);
    }
}
