import crypto from "crypto";
import JWTService from "./JWTService";

export default class UserService {
    private userInfo: any;
    private getUserInfoCb: () => Promise<any>;
    private readonly userToken: string;

    constructor(userToken?: string) {
        this.userToken = userToken;
    }

    public async generateTokensForUser(userId: string | number, data: any, expiresIn?: number): Promise<{token: string, refreshToken: string}> {
        const token = JWTService.generateToken(data, expiresIn);
        const refreshToken = JWTService.generateRefreshToken();

        return {
            token,
            refreshToken,
        };
    }

    public setUserInfoCb(cb: () => Promise<any>) {
        this.getUserInfoCb = cb;
    }

    public async getUserByToken(): Promise<any> {
        const data = this.getTokenInfo();

        if (data && data.id) {
            const user = await Db.getMongoModel("User").findById(data.id);
            return user;
        }
    }

    public getTokenInfo(): any {
        return JWTService.decodeToken(this.userToken);
    }

    public async getUserInfo(): Promise<any> {
        if (!this.userInfo) {
            if (this.getUserInfoCb) {
                this.userInfo = await this.getUserInfoCb();
            } else {
                this.userInfo = await this.getUserByToken();
            }
        }

        return this.userInfo;
    }

    public async getUserInfoOrError(): Promise<any> {
        const userInfo = this.getUserInfo();

        if (!userInfo) {
            throw new NotFound("User not found");
        }

        return userInfo;
    }

    public static encryptPass(pass) {
        return pass ? crypto.createHmac("sha1", Config.secretKey).update(pass.toString()).digest("hex") : false;
    }
}
