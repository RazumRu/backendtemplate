import Message from "./Message";

export default class Form {
    private validator: (text: string) => void;

    constructor(public text: string, public callback: (message: Message) => Promise<any>) {
    }

    public setValidator(callback: (text: string) => void) {
        this.validator = callback;
    }

    public async valid(value) {
        if (this.validator) { await this.validator(value); }
    }
}

module.exports = Form;
