import Form from "@core/classes/telegram/Form";
import {Keyboard, KeyboardData} from "@core/classes/telegram/Keyboard";
import TelegramBot from "node-telegram-bot-api";
import TelegramClient from "../clients/TelegramClient";

type invoice = {
    title: string,
    description: string,
    currency: string,
    prices: Array<TelegramBot.LabeledPrice>,
    startParameter: string,
    payload: string,
    providerToken: string,
};

export default class Message {
    set text(val) { this.textData = val; this.changesData.text = 1; }
    set photo(val) { this.photoData = val; this.changesData.photo = 1; }
    set caption(val) { this.captionData = val; this.changesData.caption = 1; }
    set keyboard(val) { this.keyboardData = val; this.changesData.keyboard = 1; }
    set inlineKeyboard(val) { this.inlineKeyboardData = val; this.changesData.inlineKeyboard = 1; }
    private callbackQueryId: string;
    private voice: TelegramBot.Voice;
    private user: TelegramBot.User;

    private textData: string;
    private photoData: string;
    private captionData: string;
    private keyboardData: Array<Array<KeyboardData>>;
    private inlineKeyboardData: Array<Array<KeyboardData>>;

    private form: Form;
    private location: Array<number>;
    private invoice: invoice;

    private removeKeyboard = false;
    private parseMode: string = "HTML";

    public args: Array<string> = [];
    public changesData: any = {};
    public messageId: string;

    constructor(public chatId: number) {
    }

    public clearChangesData() { this.changesData = {}; }

    public removeKeyboardMessage() {this.removeKeyboard = true; }

    public getInvoice() {return this.invoice; }
    public setInvoice(invoice: invoice) {this.invoice = invoice; }

    public getForm() {return this.form; }
    public setForm(form: Form) {this.form = form; }

    public getText() {return this.textData; }
    public setText(text: string) {this.text = text; }

    public setPhoto(photo: string) {this.photo = photo; }
    public getPhoto() {return this.photoData; }

    public setLocation(lat: number, lon: number) {this.location = [lat, lon]; }
    public getLocation() {return this.location; }

    public setCaption(caption: string) {this.caption = caption; }
    public getCaption() {return this.captionData; }

    public async getFileLink(telegramClient: TelegramClient) {
        let fileId = null;

        if (this.voice) {
            fileId = this.voice.file_id;
        }

        if (!fileId) { return null; }

        return telegramClient.connection.getFileLink(fileId);
    }

    public setParseMode(mode: string) {
        this.parseMode = mode;
    }

    public setArgs(args) {
        this.args = args;
    }

    public setUser(user: TelegramBot.User) {
        this.user = user;
    }

    public setCallbackId(id) {
        this.callbackQueryId = id;
    }

    public setMessageInfo(msg: TelegramBot.Message) {
        this.textData = msg.text;
        this.messageId = String(msg.message_id);
        if (msg.voice) {
            this.voice = msg.voice;
        }
    }

    public setKeyboard(keyboards: Array<Array<Keyboard>>) {
        const result = [];

        for (const line in keyboards) {
            const list = keyboards[line];

            result[line] = [];
            for (const i in list) {
                const keyboard = list[i];
                result[line][i] = keyboard.getData();
            }
        }

        this.keyboard = result;

        return this;
    }

    public setInlineKeyboard(keyboards = []) {
        const result = [];

        for (const line in keyboards) {
            const list = keyboards[line];

            result[line] = [];
            for (const i in list) {
                const keyboard = list[i];
                result[line][i] = keyboard.getData();
            }
        }

        this.inlineKeyboard = result;

        return this;
    }

    public getUserName(): string {
        return this.user && this.user.first_name;
    }

    public getUserId(): number {
        return this.user && this.user.id;
    }

    public getDataFromApi(data: any = {}): TelegramBot.SendMessageOptions {
        if (this.parseMode) {
            data.parse_mode = this.parseMode;
        }

        if (this.keyboardData) {
            data.reply_markup = {};
            data.reply_markup.keyboard = this.keyboardData;
            data.reply_markup.resize_keyboard = true;
        } else if (this.inlineKeyboardData) {
            if (!data.reply_markup) { data.reply_markup = {}; }
            data.reply_markup.inline_keyboard = this.inlineKeyboardData;
            data.reply_markup.resize_keyboard = true;
        } else if (!empty(this.removeKeyboard)) {
            if (!data.reply_markup) { data.reply_markup = {}; }
            data.reply_markup.remove_keyboard = this.removeKeyboard;
        }

        if (this.captionData) {
            data.caption = this.captionData;
        }

        return data;
    }
}
