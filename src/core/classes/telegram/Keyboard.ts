export type KeyboardData = {
    text: string,
    url?: string,
    callback_data?: string,
    request_contact?: boolean,
    request_location?: boolean,
};

export class Keyboard {
    private requestContact = false;
    private requestLocation = false;
    private url: string;

    constructor(private name: string, private callbackData?: string) {}

    public enableContact() {
        this.requestContact = true;
        return this;
    }

    public setUrl(url) {
        this.url = url;
    }

    public enableLocation() {
        this.requestLocation = true;
        return this;
    }

    public getData(): KeyboardData {
        const data: any = {
            text: this.name,
        };

        if (this.url) { data.url = this.url; }
        if (this.callbackData) { data.callback_data = this.callbackData; }
        if (this.requestContact) { data.request_contact = true; }
        if (!this.requestContact && this.requestLocation) { data.request_location = true; }

        return data;
    }
}
