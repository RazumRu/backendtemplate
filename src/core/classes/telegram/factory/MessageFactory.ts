import TelegramBot from "node-telegram-bot-api";
import Message from "../Message";

export default class MessageFactory {
    public static createMessage(msg: TelegramBot.Message, callbackQueryId?: string) {
        if (!msg) { return null; }
        // Аргументы сообщения, если это команды
        let args = [];

        let chatId = msg.chat && msg.chat.id;
        const user = msg.from;
        if (!chatId) {
            chatId = user.id;
        }

        if (msg.entities && msg.entities[0].type === "bot_command") {
            args = msg.text.split(" ");
            msg.text = args.shift();
        }

        const message = new Message(chatId);
        message.setArgs(args);
        message.setCallbackId(callbackQueryId);
        message.setMessageInfo(msg);
        message.setUser(user);

        return message;
    }

    public static createForwardMessage(chatId: number) {
        if (!chatId) {
            return null;
        }

        return new Message(chatId);
    }

}
