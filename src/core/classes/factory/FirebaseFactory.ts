import Path from "../helpers/Path";
import FirebaseService from "../services/FirebaseService";

export default class FirebaseFactory {
    private static instance;

    public static getInstance(): FirebaseService {
        if (!this.instance) {
            this.instance = new FirebaseService(Path.getPath(Config.path.files + "/firebase/stubproject-546c5-firebase-adminsdk-0path-78f6597280.json"), Config.services.firebase);
            this.instance.init();
        }

        return this.instance;
    }
}
