import EventEmitter from "events";

export default class Events extends EventEmitter {
    private static instance;

    public static getInstance(): Events {
        if (!this.instance) { this.instance = new Events(); }
        return this.instance;
    }

    public static addEvent(index, cb) {
        const instance = Events.getInstance();
        instance.on(index, cb);
    }

    public static emitInstance() {
        const instance = Events.getInstance();
        instance.emit.apply(instance, arguments);
    }

    public emit(event: string|symbol, ...args: Array<any>): boolean {
        Logger.info("Запуск эвента", event, "в кол-ве", this.listenerCount(event));
        return super.emit.call(this, event, args);
    }
}

module.exports = Events;
