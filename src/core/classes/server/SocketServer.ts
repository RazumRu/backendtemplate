import cors from "@koa/cors";
import {Socket} from "@root/node_modules/@types/socket.io";
import http from "http";
import Koa from "koa";
import io from "socket.io";

export default class SocketServer {
    public static getInstance(): SocketServer {
        if (!this.instance) { this.instance = new SocketServer(); }
        return this.instance;
    }
    private middlewares = [];

    // Bind on sockets. We gather here to be able to install before connecting
    private binds = [];
    // The same logic on io.use
    private usesBind = [];

    private onDisconnectCallbacks = [];
    private onConnectCallbacks = [];

    // io
    private connector;
    // http
    private server;
    private static instance;

    private errorHandler(err, socket) {
        Logger.error(err);
        socket.emit("err", {
            message: err.toString(),
        });
    }

    public addMiddleware(callback: (socket, ...data) => boolean) {
        this.middlewares.push(callback);
    }

    public getSocketById(id: string) {
        return this.connector.sockets.sockets[id];
    }

    /**
     * Receives all sockets of a specific group
     * @param group
     * @returns {*}
     */
    public getSocketsByGroup(group: string) {
        const namespace = this.connector.in(group);
        return $Fn(namespace.clients).cbPromise(namespace);
    }

    /**
     * Get all current connections
     * @returns {*}
     */
    public getSockets() {
        const namespace = this.connector.of("/");
        if (namespace.adapter) {
            return $Fn(namespace.adapter.clients).cbPromise(namespace);
        } else {
            return $Fn(namespace.clients).cbPromise(namespace);
        }
    }

    /**
     * Sends an event to sockets from a specific group.
     * Sends to each assigned group in turn
     * @param groups
     * @param index
     * @param data
     * @returns {*}
     */
    public emit(groups, index, data = {}) {
        Logger.debug("emit", groups, index, data);
        if (!groups) { return; }
        if (groups && !Helper.isArray(groups)) { groups = [groups]; }

        for (const group of groups) {
            this.connector.to(group).emit(index, data);
        }
    }

    /**
     * Sends an event to sockets from a specific group.
     * Sends to each assigned group in turn
     * @param socket
     * @param groups
     * @param index
     * @param data
     * @returns {*}
     */
    public emitBroadcast(socket: Socket, groups, index, data = {}) {
        Logger.debug("emitBroadcast", groups, index, data);
        if (!groups) { return; }
        if (groups && !Helper.isArray(groups)) { groups = [groups]; }

        for (const group of groups) {
            socket.broadcast.to(group).emit(index, data);
        }
    }

    public start(port = 0, host = "localhost", path = "/socket.io") {
        const app = new Koa();

        app.use(cors());

        this.server = http.createServer(app.callback());
        this.connector = io(this.server, {path});

        for (const use of this.usesBind) { this.connector.use(use); }

        Logger.sysInfo("Socket server running", port, host, path);

        this.connector.on("connection", (socket: Socket) => {
            (async () => {
                if (this.onConnectCallbacks) {
                    for (const cb of this.onConnectCallbacks) {
                        await cb.call(this, socket);
                    }
                }

                Logger.debug("Connected by socket", Object.keys(socket.rooms), process.pid);

                for (const name in this.binds) {
                    if (!this.binds.hasOwnProperty(name)) { continue; }

                    const bind = this.binds[name];
                    socket.on(name, (...args) => {
                        (async () => {
                            for (const middleware of this.middlewares) {
                                const res = await middleware.apply(null, [socket].concat(args));
                                if (!res) { return; }
                            }
                            await bind.apply(null, [socket].concat(args));
                        })().catch((err) => {
                            this.errorHandler(err, socket);
                        });
                    });
                }

                socket.once("disconnecting", () => {
                    Logger.debug("Disconnected from socket");
                    if (this.onDisconnectCallbacks) {
                        (async () => {
                            for (const cb of this.onDisconnectCallbacks) {
                                await cb.call(this, socket);
                            }
                        })();
                    }
                });

                socket.once("disconnect", () => {
                    socket.removeAllListeners();
                });
            })().catch((err) => {
                this.errorHandler(err, socket);
            });
        });

        this.server.listen(port, host);

        Events.addEvent("exit", () => {
            this.server.close();
        });
    }

    public bind(name, callback: (socket: Socket, data, cb) => Promise<any>) {
        this.binds[name] = callback;
    }

    /**
     * Аналог io.use
     * @param name
     * @param callback
     */
    public use(name, callback) {
        this.usesBind.push(callback);
    }

    public onConnect(callback: (socket: Socket) => Promise<any>, unshift = false) {
        if (unshift) {
            this.onConnectCallbacks.unshift(callback);
        } else {
            this.onConnectCallbacks.push(callback);
        }
    }

    public onDisconnect(callback: (socket: Socket) => Promise<any>, unshift = false) {
        if (unshift) {
            this.onDisconnectCallbacks.unshift(callback);
        } else {
            this.onDisconnectCallbacks.push(callback);
        }
    }
}
