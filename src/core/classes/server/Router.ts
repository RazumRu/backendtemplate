import {CallbackMiddleware, ContextData, Controller, Route} from "@core/classes/interfaces/ServerInterfaces";

export default class Router {
    private static controllers = new Map<Function, Controller>();
    private static routes = new Map<Function, Map<PropertyDescriptor, Route>>();

    /**
     * Add route
     * @param controller
     * @param path
     * @param param
     * @param descriptor
     */
    public static addRoute(controller: Function, path: string, param: {type?: string}, descriptor: PropertyDescriptor) {
        let routesList = this.routes.get(controller.constructor);

        if (!routesList) {
            routesList = new Map<PropertyDescriptor, Route>();
        }

        const route = routesList.get(descriptor);

        routesList.set(descriptor, {
            descriptor,
            type: param.type || null,
            path,
            middlewares: route ? route.middlewares : [],
        });

        this.routes.set(controller.constructor, routesList);
    }

    public static addController(target: Function, options: {path: string, typeRouter?: string, contexts?: Array<string>}) {
        const controller = this.controllers.get(target);

        this.controllers.set(target, {
            path: options.path,
            target,
            contexts: options.contexts,
            type: options.typeRouter || null,
            middlewares: controller ? controller.middlewares : [],
        });
    }

    public static addControllerMiddleware(constructor: Function, cb: CallbackMiddleware, contexts?: Array<string>) {
        const controller = this.controllers.get(constructor);
        if (controller) {
            controller.middlewares.push({cb, contexts});
        } else {
            this.controllers.set(constructor, {
                path: null,
                target: constructor,
                type: null,
                middlewares: [{cb, contexts}],
            });
        }
    }

    public static addRouteMiddleware(controller: Function, descriptor: PropertyDescriptor, cb: CallbackMiddleware, contexts?: Array<string>) {
        let routersController = this.routes.get(controller.constructor);

        if (!routersController) {
            routersController = new Map<PropertyDescriptor, Route>();
            this.routes.set(controller.constructor, routersController);
        }

        if (routersController) {
            const route = routersController.get(descriptor);

            if (route) {
                route.middlewares.push({cb, contexts});
            } else {
                routersController.set(descriptor, {
                    descriptor,
                    type: null,
                    path: null,
                    middlewares: [{cb, contexts}],
                });
            }
        }
    }

    public static getControllers() {
        return this.controllers;
    }

    public static getRoutes() {
        return this.routes;
    }
}
