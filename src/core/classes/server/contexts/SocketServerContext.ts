import {CallbackRoute, Context, ContextData} from "@core/classes/interfaces/ServerInterfaces";
import ServerCreator from "@core/classes/server/ServerCreator";
import SocketServer from "@core/classes/server/SocketServer";
import UserService from "@core/classes/services/UserService";
import url from "url";

export default class SocketServerContext implements Context {
    private socketServer;
    public contextName = CONST.CONTEXT_TYPE.SOCKETS;

    constructor(private port: number, private host: string|null,  private path: string = "/socket.io") {
        this.socketServer = SocketServer.getInstance();
    }

    public formatSendData(data: any): any {
        const time = data && Helper.isObject(data) && data.time || new Date().getTime();
        const error = data && data.error;
        if (error) { delete data.error; }

        return {
            socketData: (data && data.hasOwnProperty("result")) ? data.result : data,
            time,
            error,
        };
    }

    public addRoute(path: string, type: string|number, callback: CallbackRoute) {
        this.socketServer.bind(path, async (socket, socketData, cb) => {
            let controllerData;

            if (socketData && Helper.isObject(socketData) && socketData.socketData) {
                controllerData = socketData.socketData;
            } else {
                controllerData = socketData || {};
            }

            const contextData: ContextData = {
                contextName: this.contextName,
                context: socket,
                userService: socket.userService,
                contextInstance: this,
            };
            const data = await callback(contextData, controllerData);
            cb(this.formatSendData(data));
        });
    }

    public async run(serverCreator: ServerCreator) {
        this.socketServer.onConnect(async (socket) => {
            const token = socket.handshake.query.authorization;
            const userService = new UserService(token);
            socket.userService = userService;

            // set hostname
            socket.hostname = url.parse($String(socket.handshake.headers.origin || socket.handshake.headers.host).toUrl()).hostname;

            await $Fn(socket.join).cbPromise(socket, socket.hostname);
        }, true);

        this.socketServer.start(this.port, this.host, this.path);
    }

    public async handlerError(err: ParentError, contextData: ContextData) {
        return {
            code: err.number || err.code || 500,
            message: err.message,
        };
    }
}
