import {CallbackRoute, Context, ContextData} from "@core/classes/interfaces/ServerInterfaces";
import HttpServer from "@core/classes/server/HttpServer";
import ServerCreator from "@core/classes/server/ServerCreator";
import UserService from "@core/classes/services/UserService";
import cors from "@koa/cors";
import Router from "@root/node_modules/@types/koa-router";

export default class HttpServerContext implements Context {
    public httpServer;
    public contextName = CONST.CONTEXT_TYPE.REST;

    constructor(private port: number, private host: string|null, private prefix: string = "") {
        this.httpServer = HttpServer.getInstance();

        this.httpServer.addUse(cors());

        this.httpServer.addUse(async (ctx, next) => {
            const token = ctx.headers.authorization;
            const userService = new UserService(token);
            ctx.userService = userService;
            await next();
        });
    }

    public addUse(cb: any): void {
        this.httpServer.addUse(cb);
    }

    public formatSendData(data: any): any {
       return data;
    }

    public addRoute(path: string, type: string|number, callback: CallbackRoute) {
        let typeRoute = "get";
        switch (type) {
            case CONST.ROUTE_TYPE.POST:
            case CONST.ROUTE_TYPE.REST: typeRoute = "post"; break;
            case CONST.ROUTE_TYPE.GET: typeRoute = "get"; break;
        }

        this.httpServer.addRoute(typeRoute, this.prefix + path, async (ctx: Router.IRouterContext) => {
            // @ts-ignore
            const controllerData = $Object(ctx.params).setDefaultProps(ctx.request.body, ctx.query || {});

            const contextData: ContextData = {
                contextName: this.contextName,
                context: ctx,
                // @ts-ignore
                userService: ctx.userService,
                contextInstance: this,
            };
            const data: any = await callback(contextData, controllerData);

            if (data.error) {
                ctx.status = Helper.isNumber(data.error.code) ? data.error.code : 500;
                ctx.body = data.error;
            } else {
                ctx.body = data.result;
            }
        });
    }

    public async run(serverCreator: ServerCreator) {
        return this.httpServer.start(this.port, this.host);
    }

    public async handlerError(err: ParentError, contextData: ContextData) {
        return {
            code: err.number || err.code || 500,
            message: err.message,
        };
    }
}
