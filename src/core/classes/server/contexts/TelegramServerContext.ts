import {CallbackRoute, Context, ContextData} from "@core/classes/interfaces/ServerInterfaces";
import ServerCreator from "@core/classes/server/ServerCreator";
import UserService from "@core/classes/services/UserService";
import TelegramClient from "../../clients/TelegramClient";
import MessageFactory from "../../telegram/factory/MessageFactory";
import Message from "../../telegram/Message";

export default class TelegramServerContext implements Context {
    public telegramClient: TelegramClient;
    public contextName = CONST.CONTEXT_TYPE.TELEGRAM;

    constructor(private token: string) {
        this.telegramClient = new TelegramClient();
    }

    public formatSendData(data: any): any {
       return data;
    }

    public addRoute(path: string, type: string|number, callback: CallbackRoute) {
        const typeRoute = type || CONST.ROUTE_TYPE.STRING;

        let value: any = path;
        if (typeRoute === CONST.ROUTE_TYPE.REGEXP) {
            value = new RegExp(path, "i");
        }

        const cb = async (message: Message) => {
            const userService = new UserService();
            userService.setUserInfoCb(() => {
                return Db.getMongoModel("User")
                    .findOne({chatId: message.chatId})
                    .exec();
            });

            const contextData: ContextData = {
                contextName: this.contextName,
                context: message,
                userService,
                contextInstance: this,
            };
            await callback(contextData, {
                telegramClient: this.telegramClient,
            });
        };

        if (typeRoute === CONST.ROUTE_TYPE.REGEXP || typeRoute === CONST.ROUTE_TYPE.STRING) {
            this.telegramClient.addBind(value, cb);
        }

        if (typeRoute === CONST.ROUTE_TYPE.CALLBACK) {
            this.telegramClient.addCallback(value, cb);
        }

        if (typeRoute === CONST.ROUTE_TYPE.VOICE) {
            this.telegramClient.setOnVoice(cb);
        }

        if (typeRoute === CONST.ROUTE_TYPE.MESSAGE) {
            this.telegramClient.setOnMessage(cb);
        }
    }

    public async run(serverCreator: ServerCreator) {
        this.telegramClient.connect(this.token);
    }

    public async handlerError(err: ParentError, contextData: ContextData) {
        const message = MessageFactory.createForwardMessage(contextData.context.chatId);
        message.setText(err.message);
        return this.telegramClient.sendMessage(message);
    }
}
