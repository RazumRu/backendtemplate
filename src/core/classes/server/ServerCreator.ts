import ModuleInstance from "@core/classes/apps/ModuleInstance";
import ParentError from "@core/classes/errors/ParentError";
import Path from "@core/classes/helpers/Path";
import {CallbackRoute, CallbackRouteData, Context, ContextData, Controller, Route} from "@core/classes/interfaces/ServerInterfaces";
import Router from "@core/classes/server/Router";
import fs from "fs";

export default class ServerCreator {
    public static getInstance(): ServerCreator {
        if (!this.instance) { this.instance = new ServerCreator(); }
        return this.instance;
    }

    private static instance;

    public async callMiddlewares(controller: Controller, route: Route, contextData: ContextData, data: any) {
        const contextName = contextData.contextName;

        for (const middleware of controller.middlewares) {
            if (!middleware.contexts || $Array(middleware.contexts).search(contextName)) {
                await middleware.cb(contextData, data);
            }
        }

        for (const middlewareRoute of route.middlewares) {
            if (!middlewareRoute.contexts || $Array(middlewareRoute.contexts).search(contextName)) {
                await middlewareRoute.cb(contextData, data);
            }
        }
    }

    /**
     * Runs through modules, pulls out of them routes
     */
    public initAllRoutes() {
        let modules = require("@app/modules");
        if (modules.default) { modules = modules.default; }

        for (const path of modules) {
            const paths = [
                `@app/modules/${path}`,
                `@core/modules/${path}`,
            ];

            let status = false;
            for (const path of paths) {
                const pathAlias = Path.getPath(path);
                if (!fs.existsSync(pathAlias)) { continue; }
                let module = require(pathAlias);
                if (module.default) { module = module.default; }

                Logger.debug("init", pathAlias);
                module = new module();
                module.initControllers();
                status = true;
                break;
            }

            if (!status) { Logger.warn("module not found", path); }
        }
    }

    public async initContexts(contexts: Array<Context>) {
        const controllers = Router.getControllers();
        const routes = Router.getRoutes();

        for (const contextInstance of contexts) {
            for (const controller of controllers.values()) {
                const contexts = controller.contexts;

                if (contexts && !$Array(contexts).search(contextInstance.contextName)) {
                    continue;
                }

                const controllerRoutes = routes.get(controller.target);

                if (!controllerRoutes) {
                    continue;
                }

                for (const route of controllerRoutes.values()) {
                    let path = controller.path;
                    if (path && !path.endsWith("/")) { path += "/"; }
                    if (path) {
                        path += route.path;
                    } else {
                        path = route.path;
                    }

                    path = path.replace(/\/+/g, "/");

                    const type = route.type || controller.type || "default";
                    Logger.debug("add route", contextInstance.contextName, type, path);

                    contextInstance.addRoute(path, type, async (contextData: ContextData, data: any): CallbackRouteData => {
                        try {
                            Logger.request(`context: ${contextInstance.contextName}; ${type}: ${route.path}`);
                            await this.callMiddlewares(controller, route, contextData, data);

                            const controllerConstructor = controller.target;
                            const controllerInstance = new controllerConstructor(contextData, data);

                            return {
                                result: await route.descriptor.value.call(controllerInstance),
                                error: null,
                            };
                        } catch (err) {
                            if (!(err instanceof ParentError)) {
                                err = new SysError("System error", err);
                            }

                            if (err.name === "SysError") {
                                Logger.error(err);
                            } else {
                                Logger.warn(err);
                            }

                            return {
                                error: await contextInstance.handlerError(err, contextData),
                                result: null,
                            };
                        }
                    });
                }
            }

            await contextInstance.run(this);
        }
    }
}
