import Router from "@root/node_modules/@types/koa-router";
import http from "http";
import Koa from "koa";
import KoaRouter from "koa-router";
import formidable from "koa2-formidable";

export default class HttpServer {

    public static getInstance(): HttpServer {
        if (!this.instance) { this.instance = new HttpServer(); }

        return this.instance;
    }
    private run = false;
    private app;
    private router;
    private server;
    private static instance;

    constructor() {
        this.app = new Koa();
        this.router = new KoaRouter();
    }

    public addUse(callback) {
        this.app.use(callback);
    }

    public addRoute(method: string, path: string, callback: (ctx: Router.IRouterContext) => Promise<any>) {
        if (!path.startsWith("/")) { path = "/" + path; }
        this.router[method](path, callback);
    }

    public isRun() {
        return !!this.run;
    }

    public start(port: number, host = "localhost") {
        this.app.use(formidable({
            maxFieldsSize: Config.maxFieldsSize,
            maxFileSize: Config.maxFileSize,
            hash: "md5",
        }));

        this.app.use(this.router.routes());
        this.app.use(this.router.allowedMethods());

        this.server = http.createServer(this.app.callback());

        return new Promise((res, rej) => {
            this.server.listen(port, host, (err) => {
                if (err) { return rej(err); }
                Logger.sysInfo("Http server running " + port, host);
                this.run = true;
                res();
            });
        });
    }
}
