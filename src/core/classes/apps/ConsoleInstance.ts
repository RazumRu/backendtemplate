/**
 * parent console scripts
 */
export default abstract class ConsoleInstance {
    constructor(protected args: any) {}

    public abstract run();
}
