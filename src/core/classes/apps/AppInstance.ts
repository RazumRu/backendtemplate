import cluster from "cluster";

/**
 * Root all Instances
 */
export default abstract class AppInstance {
    private workers = {};
    private pids = [];

    constructor(protected args: any) {}

    public cluster(count, callback) {
        const processCount = count || require("os").cpus().length;

        if (cluster.isMaster && processCount > 1) {
            this.workers = {};
            this.pids = [];

            for (let i = 0; i < processCount; i += 1) {
                const worker = cluster.fork();
                this.workers[worker.process.pid] = worker;
                this.pids.push(worker.process.pid);
            }

            cluster.on("online", (worker) => {
                Logger.sysInfo(Lang.get("system.processRun", {pid: worker.process.pid}));
            });

            // when the process crashes, restart it
            cluster.on("exit", (worker, code, signal) => {
                delete this.workers[worker.process.pid];
                $Array(this.pids).removeVal(worker.process.pid);
                Logger.error(Lang.get("system.processComplete", {
                    pid: worker.process.pid,
                    code,
                    message: signal,
                }));
                cluster.fork();
            });

            callback(true);
        } else {
            callback(false);
        }
    }

    public abstract run();
}
