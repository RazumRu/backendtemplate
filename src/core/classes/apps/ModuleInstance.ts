import requireAll from "require-all";

export default abstract class ModuleInstance {
    protected constructor(protected socket, private dir: string) {}

    public getControllersPath() {
        return this.dir + "/controllers";
    }

    public initControllers() {
        requireAll(this.getControllersPath());
    }
}
