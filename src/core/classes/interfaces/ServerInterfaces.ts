import ModuleInstance from "@core/classes/apps/ModuleInstance";
import ServerCreator from "@core/classes/server/ServerCreator";
import UserService from "@core/classes/services/UserService";

export interface ContextData {
    contextName: string|number;
    contextInstance: Context;
    context: any;
    userService: UserService;
}

export type CallbackRouteData = Promise<{error?: {code: number, message: string}, result: any}>;
export type CallbackRoute = (contextData: ContextData, data: any) => CallbackRouteData;
export type CallbackMiddleware = (contextData: ContextData, data: any) => Promise<any>;

export interface Route {
    descriptor: PropertyDescriptor;
    type: string;
    path: string;
    middlewares: Array<{cb: CallbackMiddleware, contexts?: Array<string>}>;
}

export interface Controller {
    path: string;
    target: any;
    type: string;
    contexts?: Array<string>;
    middlewares: Array<{cb: CallbackMiddleware, contexts?: Array<string>}>;
}

export interface Context {
    contextName: string|number;
    addRoute(path: string, type: string|number, callback: CallbackRoute);
    handlerError(err: Error, contextData: ContextData): Promise<any>;
    run(serverCreator: ServerCreator);
    formatSendData(data: any): any;
}
