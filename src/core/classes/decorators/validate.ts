import Validator from "@core/classes/helpers/Validator";

export function Validate(variables: any) {
    return function (clazz: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const originalMethod = descriptor.value;

        descriptor.value = async function SafeWrapper() {
            const validator = new Validator(this.data);
            if (!validator.validate(variables)) {
                throw new HttpError(500, validator.getMessage());
            }

            return originalMethod.apply(this, arguments);
        };
    };
}
