import {CallbackMiddleware, ContextData} from "@core/classes/interfaces/ServerInterfaces";
import Router from "@core/classes/server/Router";

export function Controller(path?: string, type?: string, contexts?: Array<string>) {
    return function (constructor: any) {
        if (path === undefined) {
            path = constructor.name;
        }
        Router.addController(constructor, {
            path,
            contexts,
            typeRouter: type,
        });
    };
}

export function Route(path?: string, type: string = null) {
    return function (clazz: any, propertyKey: string, descriptor: PropertyDescriptor) {
        if (!path) {
            path = propertyKey;
        }

        Router.addRoute(clazz, path, {
            type,
        }, descriptor);
    };
}

export function Middleware(middleware: CallbackMiddleware, contexts?: Array<string>) {
    return function (clazz: any, propertyKey?: string, descriptor?: PropertyDescriptor) {
        if (propertyKey) {
            Router.addRouteMiddleware(clazz, descriptor, middleware, contexts);
        } else {
            Router.addControllerMiddleware(clazz, middleware, contexts);
        }
    };
}
