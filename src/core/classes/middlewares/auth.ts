import {ContextData} from "@core/classes/interfaces/ServerInterfaces";

export async function isAuth(contextData: ContextData, data: any) {
    const user = await contextData.userService.getUserInfo();

    if (empty(user)) {
        throw new HttpError(403, Lang.get("router.accessDenied", null, "Access is denied"));
    }
}

export async function isAdmin(contextData: ContextData, data: any) {
    const user = await contextData.userService.getUserInfo();

    if (empty(user) || user.type !== CONST.USER_TYPE.ADMIN) {
        throw new HttpError(403, Lang.get("router.accessDenied", null, "Access is denied"));
    }
}

export async function isGuest(contextData: ContextData, data: any) {
    const user = await contextData.userService.getUserInfo();

    if (!empty(user)) {
        throw new HttpError(403, Lang.get("router.accessDenied", null, "Access is denied"));
    }
}
