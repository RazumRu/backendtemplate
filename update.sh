#!/bin/bash

git checkout .
git clean -n 
git clean -f
git pull
redis-cli flushall
pm2 stop all
sh start_prod.sh