New migrate: `node dist/src/console migrateCreate --name nameMigration --dev`
Push new migrations: `node dist/src/console migrate --dev`
--force param clear base
--seed param run seed on create tables

Rollback migrations: `node dist/src/console migrateRollback --dev`

New seed: `node dist/src/console seedCreate --name nameSeed --dev`
Run seed: `node dist/src/console seedRun --dev`

Dump: `node dist/src/console dump --dev`


Start app: `node dist/src/start appName --dev`
Start console: `node dist/src/console consoleName --dev`

Tests: `node dist/src/test testName`