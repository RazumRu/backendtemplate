#!/usr/bin/env bash

npm install
rm -rf dist
tsc
node dist/src/test.js helpers --dev
node dist/src/test.js validator --dev
pm2 stop all
pm2 start dist/src/start.js --name core --node-args="--max_old_space_size=2048" -- core --dev
pm2 monit