#!/usr/bin/env bash

rm -rf dist
tsc
node dist/src/console.js --name test --node-args="--max_old_space_size=2048" -- test --dev
