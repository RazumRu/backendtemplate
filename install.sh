#!/bin/bash

sudo apt-get update
sudo apt-get install ssh
sudo apt-get install openssh-server
sudo apt-get install curl
sudo apt-get install nano
sudo apt-get install libpangocairo-1.0-0 libx11-xcb1 libxcomposite1 libxcursor1 libxdamage1 libxi6 libxtst6 libnss3 libcups2 libxss1 libxrandr2 libgconf2-4 libasound2 libatk1.0-0 libgtk-3-0


#nvm and nodejs
sudo apt-get install build-essential libssl-dev
# https://github.com/creationix/nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
source ~/.profile
nvm install latest
#nvm install NEW_VERSION --reinstall-packages-from=OLD_VERSION


#redis
sudo apt-get install build-essential tcl
cd /tmp
curl -O http://download.redis.io/redis-stable.tar.gz
tar xzvf redis-stable.tar.gz
cd redis-stable
make
sudo make install
sudo mkdir /etc/redis
sudo cp /tmp/redis-stable/redis.conf /etc/redis
sudo nano /etc/redis/redis.conf #supervised systemd      dir /var/lib/redis
sudo nano /etc/systemd/system/redis.service
#[Unit]
#Description=Redis In-Memory Data Store
#After=network.target
#[Service]
#User=redis
#Group=redis
#ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf
#ExecStop=/usr/local/bin/redis-cli shutdown
#Restart=always
#[Install]
#WantedBy=multi-user.target
sudo adduser --system --group --no-create-home redis
sudo mkdir /var/lib/redis
sudo chown redis:redis /var/lib/redis
sudo chmod 770 /var/lib/redis
sudo systemctl start redis
sudo systemctl status redis # test
sudo systemctl enable redis

#mongo
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo service mongod start
mongo
db.createUser({user:"razumru", pwd:"05253545", roles:[{role:"root", db:"admin"}]})
exit
sudo nano /etc/mongod.conf
#security:
 #authorization: enabled
 #
 # bindIp: 0.0.0.0
 sudo service mongod restart
 mongo -u razumru -p 05253545
 use root
 db.createUser({user: "razumru", pwd: "05253545", roles: [ "readWrite", "dbAdmin" ]})
 sudo systemctl enable mongod

#imagemagick
sudo apt-get -y install imagemagick

#rabbit
echo "deb http://www.rabbitmq.com/debian/ testing main" | sudo tee /etc/apt/sources.list.d/rabbitmq.list
curl http://www.rabbitmq.com/rabbitmq-signing-key-public.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install rabbitmq-server

#nginx
sudo apt-get update
sudo apt-get install nginx
sudo nano /etc/nginx/sites-available/default  #nginxStatic file content. Может лежать в папке conf.d
sudo systemctl enable nginx
sudo openssl req -x509 -sha256 -newkey rsa:2048 -keyout /home/razumru/libtrip/files/ssl/admin-key.pem -out /home/razumru/libtrip/files/ssl/admin-cert.pem -days 3650 -nodes #Самоподписный сертефикат
#В nginx.conf нужно добавить следующие строки для правильной буферизации
#	proxy_buffering on;
#	proxy_buffer_size 4k;
#	proxy_buffers 32 4k;
#	proxy_max_temp_file_size 0;


#git
sudo apt-get install git
sudo nano /etc/ssh/sshd_config
#PubkeyAuthentication yes
#AuthorizedKeysFile %h/.ssh/authorized_keys
#RhostsRSAAuthentication no
#HostbasedAuthentication no
#PermitEmptyPasswords no
sudo mkdir ~/.ssh
sudo chmod 700 ~/.ssh/
sudo chown $USER ~/.ssh/
nano ~/.ssh/authorized_keys
sudo chmod 600 ~/.ssh/authorized_keys
sudo chmod 600 ~/.ssh/id_rsa
sudo chmod 600 ~/.ssh/id_rsa.pub
sudo chown $USER ~/.ssh/id_rsa
sudo chown $USER ~/.ssh/id_rsa.pub



cd ~/.ssh
sudo ssh-keygen -t rsa
cat ~/.ssh/id_rsa.pub
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
service sshd restart

#iptables
iptables -F #Очистка таблицы
iptables -L #Просмотр всех правил
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT #Разрешаем на вход пакеты на все сетевые интерефейсы, приходящие в ответ на наши запросы. -D - удаление
iptables -A INPUT -i lo -j ACCEPT #Разрешаем все пакеты на локали
iptables -A INPUT -m tcp -p tcp --dport 80 -j ACCEPT #Разрешаем пакеты на определенный порт
iptables -A INPUT -m tcp -p tcp --dport 443 -j ACCEPT #Разрешаем пакеты на определенный порт
iptables -A INPUT -m tcp -p tcp --dport 22 -j ACCEPT #Разрешаем пакеты на определенный порт
iptables -A INPUT -m tcp -p tcp --dport 8080 -j ACCEPT #Разрешаем пакеты на определенный порт
iptables -A INPUT -m tcp -p tcp --dport 8088 -j ACCEPT #Разрешаем пакеты на определенный порт
iptables -A INPUT -m tcp -p tcp --dport 10000 -j ACCEPT #Разрешаем пакеты на определенный порт
iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP #блокирует нулевые пакеты
iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP #сброс пакетов XMAS
iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP #защита от syn-flood
iptables -P INPUT DROP #Сбрасывать входящие пакеты по умолчанию
iptables -P OUTPUT ACCEPT #Разрешаем все исходязие соединения
sudo apt-get install iptables-persistent #автоматическая подгрузка правил при запуске системы
iptables-save >  /etc/iptables/rules.v4 #save
sudo service netfilter-persistent start

#npm
sudo apt-get install npm

#modules node js
sudo npm cache clean
sudo npm install
sudo npm update

#webmin
sudo apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl libdigest-md5-perl
sudo apt-get install openssh-server
sudo nano /etc/apt/sources.list #добавить deb http://download.webmin.com/download/repository sarge contrib
wget http://www.webmin.com/jcameron-key.asc
sudo apt-key add jcameron-key.asc
sudo apt-get update
sudo apt-get install webmin



#project..
sudo mkdir /home/razumru/libtrip
cd /home/razumru/libtrip
git clone git@bitbucket.org:RazumRu/JarvisIO.git .
git checkout master
git fetch --all
#git checkout . - удалить изменения в файлах
#git clean -n
#git clean -f

#alsa
sudo apt-get install pulseaudio alsa alsa-utils alsamixer
sudo adduser ${USER} audio
sudo reboot
alsamixer
# arecord output.wav  -f cd


#srlim
#1 - download http://www.speech.sri.com/projects/srilm/download.html
mkdir /home/razumru/jarvis/lib/srlim
tar -xvf srilm-1.7.2.tar.gz -C /home/razumru/jarvis/lib/srlim
sudo nano srlim/Makefile #SRILM = /home/razumru/jarvis/lib/srlim
sudo make
# /home/tools/user/srilm/bin/[your_machine_type]/ = srlim

#ffmpeg
# download https://www.ffmpeg.org/download.html
mkdir /home/razumru/jarvis/lib/ffmpeg
tar -xvf ffmpeg-4.1.tar.bz2 -C /home/razumru/jarvis/lib/ffmpeg
sudo apt-get install pkg-config
./configure --disable-x86asm
make
sudo make install

#pocketsphinx
sudo apt-get install alsa-utils
sudo apt-get install bison
sudo apt-get install libasound2-dev
sudo apt-get install sox
sudo apt-get install gcc automake autoconf libtool bison python-dev libpulse-dev
sudo apt-get install libpcre3-dev
wget -O swig-3.0.12.tar.gz https://downloads.sourceforge.net/project/swig/swig/swig-3.0.12/swig-3.0.12.tar.gz?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fswig%2Ffiles%2Fswig%2Fswig-3.0.12%2Fswig-3.0.12.tar.gz%2Fdownload&ts=1486782132&use_mirror=superb-sea2
tar xf swig-3.0.12.tar.gz
cd swig-3.0.12
./configure --prefix=/usr
make -j 4
sudo make install

#download https://cmusphinx.github.io/wiki/download/
tar -xvf sphinxbase-5prealpha.tar.gz
tar -xvf pocketsphinx-5prealpha.tar.gz
sudo sh ./autogen.sh
sudo make
sudo make install
sudo apt-get install pavucontrol











